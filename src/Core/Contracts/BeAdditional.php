<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.10
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Contracts;

interface BeAdditional
{
    /**
     * @param mixed $data
     * */
    public function __construct($data);

    /** get all additional data
     * @return mixed
     * */
    public function getData();
}
