<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.9
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

use Illuminate\Support\Str;

trait RelationsHelper
{
    /**
     * @var string $defaultNameSpace
     * */
    protected $defaultNameSpace = 'Lantana\\Models\\';
    /**
     * @var array $relationsAssociative
     * */
    protected $relationsAssociative = [];
    /**
     * @var array|\Lantana\Extensions\Collection\Collection|null $cachedEmbedding
     * */
    protected $cachedEmbedding;

    /** used in WhereClosureHelper magic methods
     * @param string $field
     * @param mixed $value
     * @return mixed
     * */
    private function getBy($field, $value)
    {
        return $this->findByFieldValue($field, $value)->get();
    }

    /**
     * @param string      $entity
     * @param string|null $foreign
     *
     * @return $this
     */
    final public function with(string $entity, string $foreign = null)
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->data()->with($entity, $foreign ?: "{$entity}_uuid");

        return $this;
    }

    /**
     * @param string $entity
     * @param null|string $local_key
     *
     * @return $this
     */
    final public function belongs($entity, $local_key = null)
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->data()->belongs($entity, $local_key ?: "{$this->entity}_uuid");

        return $this;
    }

    /** get relation with for model class full name
     * @param string $model
     * @param string $foreign
     * @return $this
     * */
    final public function withModel(string $model, string $foreign = null)
    {
        return $this->with($this->getEntityFromModelName($model), $foreign);
    }

    /** get relation belongs for model class full name
     * @param string $model
     * @param string $local_key
     * @return $this
     * */
    final public function belongsToModel(string $model, string $local_key = null)
    {
        return $this->belongs($this->getEntityFromModelName($model), $local_key);
    }

    /** get relation collection for any entity (overrides)
     * @param string $class
     * @param string $local_key
     * @return \Lantana\Extensions\Collection\Collection
     * */
    protected function hasMany(string $class, string $local_key = null)
    {
        $entity = $this->getEntityFromModelName($class);

        if (isset($this->{$entity})) {
            return $this->{$entity};
        }

        return $class::where($local_key ?? $this->getEntityFromModelName(get_class($this)) . '_uuid', $this->uuid)->get();
    }

    /** get relation model for any entity (overrides)
     * @param string $class
     * @param string $foreign
     * @return \Lantana\Model|mixed|null
     * */
    protected function belongsTo(string $class, string $foreign = null)
    {
        $entity = $this->getEntityFromModelName($class);

        if (isset($this->{$entity})) {
            return $this->{$entity};
        }

        return $class::where($foreign ?? 'uuid', $this->{$entity . '_uuid'})->first();
    }


    /** helper for withModel and belongsToModel methods
     * @param string $model
     * @return string
     * */
    private function getEntityFromModelName(string $model): string
    {
        $smallClass = last(explode('\\', $model));
        $entity = Str::kebab(mb_substr($smallClass, 2)); // substr 'JS'

        $this->relationsAssociative[$entity] = $model;

        return $entity;
    }

    /** get full name model for kebab name entity
     * @param string $entity
     * @return string
     * */
    private function getModelName($entity)
    {
        return $this->relationsAssociative[$entity] ?? $this->defaultNameSpace . 'JS' . Str::ucfirst(Str::camel($entity));
    }
}
