<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

trait ParsingHelper
{
    /** helper parsing construct params
     * @param array $args
     * @param string $uri
     * @param string $apikey
     * @param mixed $data
     *
     * @return $this
     */
    private function parseArgs($args, &$uri, &$apikey, &$data)
    {
        foreach ($args as $v) {
            if (is_array($v) || is_object($v)) {
                $data = (object)$v;
                continue;
            }
            if (preg_match("/^[0-9a-fA-F]{32}$/", $v)) {
                $apikey = $v;
                continue;
            }
            if (preg_match("/^http[s]{0,1}:\/\//", $v)) {
                $uri = $args[0];
            }
        }
        return $this;
    }

    /** helper get entity from class name
     * @return string|null
     */
    final public function getEntity()
    {
        if (!isset($this->entity)) {
            $t = explode('\\', get_class($this));
            $t = (string)end($t);
            $t = preg_split('/(?=[A-Z])/', $t, -1, PREG_SPLIT_NO_EMPTY);
            $t = implode('-', array_map(function ($v) {
                return mb_strtolower((string)$v);
            }, array_slice((array)$t, 2)));

            $this->entity = $t;
        }
        return $this->entity;
    }

    /** serialise model to JSON
     * @override JsonSerializable
     * @return array|\stdClass
     * */
    public function jsonSerialize()
    {
        return (array)$this->data;
    }

    public function __toString()
    {
        return (string)json_encode($this->data);
    }
}
