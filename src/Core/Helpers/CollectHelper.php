<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Extensions\Collection\Collection;

trait CollectHelper
{
    /** constructing collections from data
     * @param mixed $data
     * @param string $model
     * @return mixed
     * */
    public function initDataCollection($data, $model = null)
    {
        if (is_null($data)) {
            return null;
        }

        /** for one model
         * */
        if (isset($data->uuid)) {
            if (isset($data->_embedded)) {

                /** constructing collections and|or models in _embedded
                 * */
                $data->_embedded = (object)array_reduce(
                    array_map(function ($key, $value) {
                        return [$key, $this->initDataCollection($value, $this->getModelName($key))];
                    }, array_keys((array)$data->_embedded), (array)$data->_embedded),
                    function ($result, $item) {
                        [$key, $value] = $item;
                        $result[$key] = $value;
                        return $result;
                    },
                    []
                );
            }

            /** constructing and return model for _embedded
             * */
            if (isset($model)) {
                /**
                 * @var \Lantana\Core\Request\RequestParams $requestParams
                 * */
                $requestParams = $this->request_params;
                return new $model($requestParams->uri(), $requestParams->apikey(), $data);
            }

            /** return main data (top level)
             * */
            return $data;

        /** for collecting models
         * */
        } else {

            /** store collection from array $data
             * */
            return new Collection(
            /** delete uuid key in all array
             * */
                array_values(
                    array_map(function ($value) use ($model) {
                        return $this->initDataCollection($value, $model ?? get_class($this));
                    }, (array)$data)
                )
            );
        }
    }

    /** control all rules before start request mass actions (for Collection)
     * @param Collection $collection
     * @param string $method
     * @return void
     * @throws LantanaException
     * */
    protected static function controlValidCollectBeforeRequest(Collection $collection, string $method)
    {
        if (!static::isValidTypeCollectItems($collection)) {
            throw new LantanaException('Types items should be equals');
        }
        if ($collection->isEmpty()) {
            throw new LantanaException('Collection is empty');
        }
        static::isValidParamsModelsCollect($collection, $method);
    }

    /** validator items for equals types
     * @param Collection $collection
     * @return bool
     * */
    private static function isValidTypeCollectItems(Collection $collection): bool
    {
        $type = null;
        return $collection->every(function ($item) use (&$type) {
            isset($type) ?: $type = get_class($item);
            return get_class($item) == $type;
        });
    }

    /** validator items for valid inner models params where need this method
     * @param Collection $collection
     * @param string $method
     * @return void
     * @throws LantanaException
     * */
    private static function isValidParamsModelsCollect(Collection $collection, string $method)
    {
        switch ($method) {

            /** model param uuid should be undefined or be null
             * P.S.: (07.09.2019) server JStorage support mass store only for model param uuid is undefined
             * */
            case 'store':
                if ($collection->every(function ($item) {
                    if (empty($item->uuid)) {
                        unset($item->data->uuid);
                        return true;
                    }
                    return false;
                })) {
                    return;
                }

                throw new LantanaException('Method [' . $method . ']: Param uuid every models in collection should be undefined or be null');

            /** model param uuid should be required
             * */
            case 'update':
            case 'destroy':
                if ($collection->every(function ($item) {
                    return !empty($item->uuid);
                })) {
                    return;
                }

                throw new LantanaException('Method [' . $method . ']: Param uuid every models in collection should be required');
        }

        throw new LantanaException('Method [' . $method . '] is invalid');
    }
}
