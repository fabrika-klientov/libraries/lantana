<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.17
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

/**
 * statics methods for getting, updating, deleting model(s)
 * store new model instance
 * */
trait ContextualHelper
{
    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public static function __callStatic($name, $arguments)
    {
        $context = new static();
//        $context = new static('http://localhost:8001', 'b9b7089041493589bdaa81c079dba2f0'); // !!! TEST  delete this params

        switch ($name) {
            case 'where':
                return $context->where(...$arguments);
            case 'wherein':
                return $context->wherein(...$arguments);
            case 'select':
                return $context->select(...$arguments);
            case 'all':
                return $context->all();
            case 'find':
                return $context->find(...$arguments);
        }

        return null;
    }
}
