<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

/**
 * @method $this where($field, $value)
 * @method $this wherein($field, array $value)
 * @method $this select($fields = [])
 * @method $this find($uuid)
 * @method $this findByFieldValue($field, $value)
 * @method \Lantana\Extensions\Collection\Collection all() alias method for get()
 * */
trait WhereClosureHelper
{
    /**
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * @throws \Lantana\Core\Exceptions\LantanaException
     * */
    public function __call($name, $arguments)
    {
        if (strpos($name, 'getBy') === 0) {
            $name = substr($name, 5);
            preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $name, $matches);
            $ret = $matches[0];
            foreach ($ret as &$match) {
                $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
            }
            return call_user_func_array(
                [$this, 'getBy'],
                array_merge([implode('_', $ret)], $arguments)
            );
        }

        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;

        switch ($name) {
            /**
             * where closure
             * */
            case 'where':
                $requestData->method('getall')->apply()->where(...$arguments);
                return $this;
            case 'wherein':
                $requestData->method('getall')->apply()->wherein(...$arguments);
                return $this;
            case 'select':
                $requestData->data()->only(empty($arguments) ? [] : current($arguments));
                return $this;
            /** closure
             * ---------------
             * */

            /**
             * alias for get
             * */
            case 'all':
                return $this->get();
            /**
             * alias for findByUUID
             * */
            case 'find':
                return $this->findByUUID(...$arguments);
            /**
             * alias for where
             * */
            case 'findByFieldValue':
                return $this->where(...$arguments);
        }

        return null;
    }
}
