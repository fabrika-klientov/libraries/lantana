<?php
/**
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.9
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Helpers;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Extensions\Collection\Collection;

/**
 * @property mixed $stored
 * */
trait MassActionsHelper
{
    /** assoc methods
     * @var array $massRemoteMethods
     * */
    protected static $massRemoteMethods = [
        'store' => 'storeall',
        'update' => 'updateall',
        'destroy' => 'deleteall',
    ];


    /** mass actions
     * */

    /**
     * @param Collection $collection
     * @param bool $strict if true -> all model should be stored, else Exception
     * @return Collection
     * @throws LantanaException
     * */
    public static function store(Collection $collection, bool $strict = true)
    {
        $prototype = static::doMassRequest($collection, 'store', $strict);
        return $prototype->initDataCollection($prototype->stored ?? []);
    }

    /**
     * @param Collection $collection
     * @param bool $strict if true -> all model should be updated, else Exception
     * @return Collection
     * @throws LantanaException
     * */
    public static function update(Collection $collection, bool $strict = true)
    {
        $prototype = static::doMassRequest($collection, 'update', $strict);
        return $collection->filter(function ($item) use ($prototype) {
            return empty($prototype->updated) ? false : in_array($item->uuid, $prototype->updated);
        });
    }

    /**
     * @param Collection $collection
     * @param bool $strict if true -> all model should be removed, else Exception
     * @return Collection
     * @throws LantanaException
     * */
    public static function destroy(Collection $collection, bool $strict = true)
    {
        $prototype = static::doMassRequest($collection, 'destroy', $strict);
        return $collection->filter(function ($item) use ($prototype) {
            return empty($prototype->deleted) ? false : in_array($item->uuid, $prototype->deleted);
        });
    }

    /** helper for mass request
     * @param Collection $collection
     * @param string $method
     * @param bool $strict
     * @return static
     * @throws LantanaException
     * */
    private static function doMassRequest(Collection $collection, string $method, bool $strict = true)
    {
        static::controlValidCollectBeforeRequest($collection, $method);
        $prototype = new static();
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $prototype->request_data;
        $requestData->apply()->strict($strict);
        $requestData->method(static::$massRemoteMethods[$method])->data()->modelData($collection->toArray());
        $prototype->doRequest();

        return $prototype;
    }
}
