<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.9
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Exceptions;

use Lantana\Core\Exceptions\Additionals\Additional;
use Throwable;

class LantanaException extends \Exception
{
    /** additional data
     * @var mixed $data
     * */
    protected $data;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = 'LANTANA::CORE: ' . $message;
    }

    /** set additional data in exception
     * @param  mixed $data
     * @return void
     * */
    public function setData($data)
    {
        $this->data = $data;
    }

    /** get additional data in exception
     * @return mixed
     * */
    public function getData()
    {
        $additional = new Additional($this->data);
        return $additional->handle() ?? $this->data;
    }
}
