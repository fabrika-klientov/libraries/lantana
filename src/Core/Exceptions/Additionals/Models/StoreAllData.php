<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.10
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Exceptions\Additionals\Models;

use Lantana\Core\Contracts\BeAdditional;
use Lantana\Extensions\Collection\Collection;

/**
 * @property-read Collection $stored
 * @property-read Collection $skipped
 * */
class StoreAllData implements BeAdditional
{
    private $data;

    /**
     * @param mixed $data
     * @return void
     * */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /** get src data
     * @return mixed
     * */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $name
     * @return Collection|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'stored':
                return new Collection($this->data->stored ?? []);
            case 'skipped':
                return new Collection($this->data->skipped ?? []);
        }

        return null;
    }
}
