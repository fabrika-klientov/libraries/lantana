<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.10
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Exceptions\Additionals;

use Lantana\Core\Contracts\BeAdditional;
use Lantana\Core\Exceptions\Additionals\Models\DestroyAllData;
use Lantana\Core\Exceptions\Additionals\Models\StoreAllData;
use Lantana\Core\Exceptions\Additionals\Models\UpdateAllData;

class Additional
{
    private $data;

    /**
     * @param mixed $data
     * @return void
     * */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /** handler for get Additional data
     * @return BeAdditional|null
     * */
    public function handle()
    {
        if (empty($this->data) || !is_object($this->data)) {
            return null;
        }

        /** storeall
         * */
        if (isset($this->data->stored)) {
            return new StoreAllData($this->data);
        }

        /** updateall
         * */
        if (isset($this->data->updated)) {
            return new UpdateAllData($this->data);
        }

        /** deleteall
         * */
        if (isset($this->data->deleted)) {
            return new DestroyAllData($this->data);
        }

        return null;
    }
}
