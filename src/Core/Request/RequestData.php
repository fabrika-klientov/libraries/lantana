<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.9
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Request;

/**
 * Class RequestData
 * @package Lantana
 *
 * @method $this method(string $val = '') "get|set value"
 * @method string|$this key(string $val = '') "get|set value"
 * @method string|$this uuid(string $val = '') "get|set value"
 * @method Data data() "get value"
 * @method Data apply() "get value"
 * @method $this responsewithmodel(bool $data)
 *
 */
class RequestData
{
    /**
     * @var string $method
     */
    protected $method = '';

    /**
     * @var string $key
     */
    protected $key = null;

    /**
     * @var string $uuid
     */
    protected $uuid = '';

    /**
     * @var Data $data
     */
    protected $data = null;

    /**
     * @var Data $apply
     */
    protected $apply = null;

    /**
     * @param string $method
     * @param string $key
     */
    public function __construct(string $method = null, string $key = null)
    {
        $this->data = new Data(['with', 'only', 'belongs']);
        $this->apply = new Data(['where', 'wherein', 'responsewithmodel', 'strict']);

        if (!is_null($method)) {
            $this->method($method);
        }

        if (!is_null($key)) {
            $this->key($key);
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        if (in_array($name, ['data', 'apply'])) {
            return $this->{$name};
        }
        if (property_exists($this, $name)) {
            if (empty($arguments)) {
                return $this->{$name};
            }

            $this->{$name} = $arguments[0];
        }
        return $this;
    }

    /**
     * @return array
     */
    public function return()
    {
        $request = [
            'method' => $this->method,
            'key' => $this->key,
        ];

        if (!empty($this->uuid)) {
            $request['uuid'] = $this->uuid;
        }

        if (!empty($this->data()->return())) {
            $request['data'] = $this->data()->return();
        }

        if (!empty($this->apply()->return())) {
            $request['apply'] = $this->apply()->return();
        }

        return $request;
    }
}
