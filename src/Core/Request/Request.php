<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Request;

use Exception;
use GuzzleHttp\Client;
use Lantana\Core\Exceptions\LantanaException;

/**
 * Class Request
 * @package Lantana
 *
 * @method $this url(string $val = '') "get|set value"
 * @method $this data(array $val = []) "get|set value"
 */
class Request
{
    /**
     * @var string|null $url
     * */
    protected $url = null;
    /**
     * @var array|null $data
     * */
    protected $data = null;

    /**
     * @param string $url
     * @param array $data
     * @return void
     * */
    public function __construct(string $url = '', array $data = [])
    {
        $this->url($url);
        $this->data($data);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        if (property_exists($this, $name)) {
            if (empty($arguments)) {
                return $this->{$name};
            }

            $this->{$name} = $arguments[0];
        }
        return $this;
    }

    /**
     * @return mixed
     * @throws LantanaException
     * */
    public function post()
    {
        if (empty($this->url) || empty($this->data)) {
            throw new LantanaException('No URl or DATA for request');
        }
        try {
            $response = (new Client(['verify' => false]))->post(
                $this->url,
                [
                    'json' => $this->data,
                ]
            );
        } catch (Exception $e) {
            return [];
        }

        if (strpos($response->getHeader('Content-Type')[0], "application/json") !== false) {
            return json_decode($response->getBody()->getContents());
        }
        return [];
    }
}
