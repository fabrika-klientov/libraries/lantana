<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Request;

/**
 * Class RequestParams
 * @package Lantana
 *
 * @method string|$this uri(string $val='')
 * @method string|$this apikey(string $val='')
 * @method string|$this path(string $val='')
 *
 */
class RequestParams
{
    /**
     * @var string $uri
     */
    protected $uri = '';

    /**
     * @var string $path
     */
    protected $path = '/api/v1/data/';

    /**
     * @var string $apikey
     */
    protected $apikey = null;

    /**
     * @param string $uri
     * @param string $apikey
     * @return void
     * */
    public function __construct(string $uri = null, string $apikey = null)
    {
        if (!is_null($uri)) {
            $this->uri($uri);
        }

        if (!is_null($apikey)) {
            $this->apikey($apikey);
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        if (property_exists($this, $name)) {
            if (empty($arguments)) {
                return $this->{$name};
            }

            $this->{$name} = $arguments[0];
        }
        return $this;
    }
}
