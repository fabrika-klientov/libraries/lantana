<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Core\Request;

/**
 * Class Data
 * @package Lantana
 *
 * @method $this modelData(object|array $data = null) "Setting priority not described data"
 * @method $this responsewithmodel(bool $data)
 * @method $this where($field, $value)
 * @method $this wherein($field, array $value)
 * @method $this only($fields)
 * @method $this with($table, $key)
 * @method $this belongs($table, $key)
 * @method $this strict($strict)
 */
class Data
{
    /**
     * @var array $attributes
     * */
    protected $attributes = [];
    /**
     * @var array|null $return
     * */
    protected $return = null;
    /**
     * @var string|null $modelData
     * */
    protected $modelData = null;

    /**
     * @param array|null $attributes
     * @return self
     * */
    public function __invoke(array $attributes = null)
    {
        return new self($attributes);
    }

    /**
     * @param array|null $attributes
     * @return void
     * */
    public function __construct(array $attributes = null)
    {
        if (!empty($attributes)) {
            foreach ($attributes as $v) {
                $this->attributes[$v] = [];
            }
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        if ($name === 'modelData') {
            $this->{$name} = $arguments[0] ?? null;
            return $this;
        }

        if (array_key_exists($name, $this->attributes)) {
            if (empty($arguments)) {
                return $this->attributes[$name];
            }

            $this->reset();

            if (count($arguments) > 1 && is_string($arguments[0])) {
                switch ($name) {
                    case 'with':
                    case 'belongs':
                        $this->attributes[$name][$arguments[0]] = $arguments[1];
                        break;
                    case 'wherein':
                        $this->attributes[$name] = [$arguments[0], $arguments[1]];
                        break;
                    default:
                        $this->attributes[$name][] = $arguments;
                }
                return $this;
            }
            $this->attributes[$name] = $arguments[0];
        }

        return $this;
    }

    /**
     * @return array|null
     * */
    public function return()
    {
        if ($this->return !== null) {
            return $this->return;
        }
        if ($this->modelData === null) {
            $this->return = array_filter(
                $this->attributes,
                static function ($v) {
                    return !empty($v);
                }
            );
        } else {
            $this->return = json_decode((string)json_encode($this->modelData), true);
        }

        return $this->return;
    }

    /**
     * @return void
     * */
    public function reset(): void
    {
        $this->return === null ?: $this->return = null;
        $this->modelData === null ?: $this->modelData = null;
    }
}
