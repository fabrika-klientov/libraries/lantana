<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @author    sHa <sha@shadoll.dev>
 * @copyright 2019 Fabrika-Klientov
 * @version   19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana;

use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;
use JsonSerializable;
use Lantana\Core\Exceptions\LantanaException;
use Lantana\Core\Helpers\CollectHelper;
use Lantana\Core\Helpers\ContextualHelper;
use Lantana\Core\Helpers\MassActionsHelper;
use Lantana\Core\Helpers\ParsingHelper;
use Lantana\Core\Helpers\RelationsHelper;
use Lantana\Core\Helpers\WhereClosureHelper;
use Lantana\Core\Request\Request;
use Lantana\Core\Request\RequestData;
use Lantana\Core\Request\RequestParams;
use stdClass;

/**
 * @property-read string $uuid
 * @property-read RequestData   $request_data
 * @property-read RequestParams $request_params
 * @property-read mixed        $data
 *
 * @method static $this where($field,  $value, ...$args) (available static call)
 * @method static $this wherein($field, array $value) (available static call)
 * @method static $this select($fields = []) (available static call)
 * @method static $this|null find($uuid) (available static call)
 * @method $this findByFieldValue($field, $value)
 * @method static \Lantana\Extensions\Collection\Collection all() alias method for get() (available static call)
 */
abstract class Model implements JsonSerializable, Arrayable
{
    use ParsingHelper, WhereClosureHelper, RelationsHelper, ContextualHelper, CollectHelper, MassActionsHelper;

    /**
     * @var string
     */
    protected $apipath = '/api/v1/data/';

    /**
     * @var \Lantana\Core\Request\RequestParams|null
     */
    protected $request_params = null;

    /**
     * @var \Lantana\Core\Request\RequestData|null
     */
    protected $request_data = null;

    /**
     * @var string|null
     */
    protected $entity = null;

    /**
     * @var stdClass|null
     */
    protected $data = null;


    /**
     * Base constructor.
     *
     * @param mixed ...$args
     *
     * @throws LantanaException
     */
    public function __construct(...$args)
    {
        $uri = null;
        $apikey = null;
        $data = null;
        $this->parseArgs($args, $uri, $apikey, $data);

        !empty($uri) ?: $uri = getenv('JSTORAGE_URI');
        if (empty($uri)) {
            throw new LantanaException('[' . __CLASS__ . '] Creating model without uri info!', 1);
        }

        !empty($apikey) ?: $apikey = getenv('JSTORAGE_APIKEY');
        if (empty($apikey)) {
            throw new LantanaException('[' . __CLASS__ . '] Creating model without apikey info!', 2);
        }

        if ($data) {
            $this->data = $data;
        }

        $this->request_params = new RequestParams($uri, $apikey);
        $this->request_data = new RequestData(null, $apikey);

        $this->entity = $this->getEntity();
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {
        !is_null($this->data) ?: $this->data = new stdClass();
        $this->data->{$name} = $value;
    }

    /**
     * @param string $name
     * @return mixed|null
     * @throws LantanaException
     */
    public function __get($name)
    {
        try {
            /** search in $this->data
             * */
            if (isset($this->data->{$name})) {
                return $this->data->{$name};
            }
            /** search in $this
             * */
            if (property_exists($this, $name)) {
                return $this->{$name};
            }
            /** search in $this->data->_embedded
             * */
            if (isset($this->data, $this->data->_embedded, $this->data->_embedded->{$name})) {
                return $this->data->_embedded->{$name};
            }
            /**
             * ex.: sharedApikeys -> shared-apikeys
             * $sharedCustomersServices->{'shared-apikeys'} == $sharedCustomersServices->sharedApikeys
             * */
            $kebab = Str::kebab($name);
            if (isset($this->data, $this->data->_embedded, $this->data->_embedded->{$kebab})) {
                return $this->data->_embedded->{$kebab};
            }
            /**
             * @deprecated
             * */
            if (method_exists($this, "get" . ucfirst($name))) {
                return $this->{"get" . ucfirst($name)}();
            }
        } catch (Exception $exception) {
            throw new LantanaException($exception->getMessage());
        }

        return null;
    }

    /**
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        /** for isset
         * ex.: sharedApikeys -> shared-apikeys
         * $sharedCustomersServices->{'shared-apikeys'} == $sharedCustomersServices->sharedApikeys
         * */
        $kebab = Str::kebab($name);
        return isset($this->data, $this->data->{$name})
            || isset($this->data, $this->data->_embedded, $this->data->_embedded->{$name})
            || isset($this->data, $this->data->_embedded, $this->data->_embedded->{$kebab});
    }

    /** cloning the model
     * */
    public function __clone()
    {
        unset($this->data->uuid);
        /**
         * @var \Lantana\Core\Request\RequestParams $requestParams
         * */
        $requestParams = $this->request_params;
        /**
         * @var string $uri
         * */
        $uri = $requestParams->uri();
        /**
         * @var string $apikey
         * */
        $apikey = $requestParams->apikey();
        $this->request_params = new RequestParams($uri, $apikey);
        $this->request_data = new RequestData(null, $apikey);
    }

    //* changing methods

    /** save model in JStorage
     * @return bool
     * @throws LantanaException
     */
    final public function save()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->data()->reset();

        $requestData->apply()->responsewithmodel(true);

        $requestData->method($this->uuid ? 'update' : 'store')->data()->modelData($this->data);

        !$this->uuid ?: $requestData->uuid($this->uuid);

        $this->cachedEmbedding = $this->data->_embedded ?? null;

        $this->doRequest();

        $this->data = $this->data->{'model'};
        $this->data->_embedded = $this->cachedEmbedding;

        return true;
    }

    /** deleting this model
     * @return bool status
     * @throws LantanaException
     * */
    final public function delete()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->data()->reset();

        $requestData->method('delete')->uuid($this->uuid);

        $this->doRequest();

        $this->data = null;

        return true;
    }

    //* end changing methods

    //* select methods

    /** get closure data collection
     * @return \Lantana\Extensions\Collection\Collection
     * @throws LantanaException
     */
    final public function get()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->method('getall');
        $this->doRequest();
        $this->data = $this->initDataCollection($this->data);

        return $this->data;
    }

    /** get model for uuid
     * @param string $uuid
     * @return $this|null
     * @throws LantanaException
     */
    public function findByUUID($uuid)
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->method('get')->uuid($uuid);

        return $this->getCurrentModel();
    }

    /** get first model
     * @return $this|null
     * @throws LantanaException
     */
    final public function first()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->method('first');

        return $this->getCurrentModel();
    }

    /** get last model
     * @return $this|null
     * @throws LantanaException
     */
    final public function last()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->method('last');

        return $this->getCurrentModel();
    }

    //* end select methods

    /**
     * get data as Array
     * @return array
     * */
    public function toArray()
    {
        return json_decode(json_encode($this->data)?:'', true);
    }


    /** helper
     * @return $this|null
     * @throws LantanaException
     * */
    private function getCurrentModel()
    {
        $this->doRequest();

        if (empty($this->data)) {
            return null;
        }
        $this->data = $this->initDataCollection($this->data);

        return $this;
    }

    /** request to JStorage
     * @return void
     * @throws LantanaException
     */
    private function doRequest()
    {
        /**
         * @var \Lantana\Core\Request\RequestParams $requestParams
         * */
        $requestParams = $this->request_params;
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;

        /**
         * @var string $uri
         * */
        $uri = $requestParams->uri();
        /**
         * @var string $path
         * */
        $path = $requestParams->path();

        $res = (new Request())
            ->url($uri . $path . $this->getEntity())
            ->data($requestData->return())
            ->post();
        if (isset($res->error) && $res->error) {
            $message = '';
            if (isset($res->message)) {
                $message = $res->message;
            }
            $lantanaException = new LantanaException('Response has error: ' . $message);
            $lantanaException->setData($res);
            throw $lantanaException;
        }

        $this->data = $res;
    }
}
