<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Collection;

use Illuminate\Support\Collection as BaseCollection;
use Lantana\Core\Exceptions\LantanaException;
use Lantana\Extensions\Collection\Contracts\BeCollectionModel;

class Collection extends BaseCollection
{

    /**
     * initialise new collection for interface BeCollectionModel
     * @deprecated
     * @param BeCollectionModel $collectionModel
     * @return self
     * */
    public static function initialize(BeCollectionModel $collectionModel)
    {
        $collection = new self($collectionModel->data);
        $class = get_class($collectionModel);
        $requestParams = $collectionModel->request_params;

        $collection->transform(function ($item) use ($class, $requestParams) {
            return new $class($requestParams->uri(), $requestParams->apikey(), $item);
        });

        return $collection;
    }

    /**
     * @param bool $strict
     * @return Collection
     * @throws \Lantana\Core\Exceptions\LantanaException
     * */
    public function store(bool $strict = true)
    {
        $prototype = $this->getTypeModel();
        return $prototype::store($this, $strict);
    }

    /**
     * @param bool $strict
     * @return Collection
     * @throws \Lantana\Core\Exceptions\LantanaException
     * */
    public function update(bool $strict = true)
    {
        $prototype = $this->getTypeModel();
        return $prototype::update($this, $strict);
    }

    /**
     * @param bool $strict
     * @return Collection
     * @throws \Lantana\Core\Exceptions\LantanaException
     * */
    public function destroy(bool $strict = true)
    {
        $prototype = $this->getTypeModel();
        return $prototype::destroy($this, $strict);
    }

    /** get full name class model for this collection
     * @throws \Lantana\Core\Exceptions\LantanaException
     * @return string
     * */
    protected function getTypeModel()
    {
        if ($this->isEmpty()) {
            throw new LantanaException('Collection is empty');
        }

        $class = get_class($this->first());

        if (!class_exists($class)) {
            throw new LantanaException('Class [' . $class . '] is not found');
        }

        return $class;
    }
}
