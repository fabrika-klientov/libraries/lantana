<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Collection\Contracts;

/**
 * @property mixed $data
 * @property mixed $request_params
 * */
interface BeCollectionModel
{
}
