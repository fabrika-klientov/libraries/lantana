<?php
/**
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 * */

namespace Lantana\Extensions\Anemone;

use Closure;
use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedIntegrations;
use Lantana\Models\JSSharedOauth;

class OAuth2Service
{
    /**
     * @var static $context
     * */
    private static $context;
    /**
     * @var \Illuminate\Support\Collection $collect
     * */
    protected static $collect;

    /**
     * @return void
     * */
    public function __construct()
    {
        static::$context = $this;
        static::$collect = collect();
    }

    /**
     * @param JSSharedAmocrm $sharedAmocrm
     * @param JSSharedOauth $sharedOauth
     * @param JSSharedIntegrations $sharedIntegrations
     * @return $this
     * */
    public function add(
        JSSharedAmocrm $sharedAmocrm,
        JSSharedOauth $sharedOauth,
        JSSharedIntegrations $sharedIntegrations
    ) {
        static::$collect->push(
            [
                'amocrm' => $sharedAmocrm,
                'oauth' => $sharedOauth,
                'integration' => $sharedIntegrations,
            ]
        );

        return $this;
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function items()
    {
        return static::$collect;
    }

    /**
     * @param Closure|string $amoDomain
     * @return \Illuminate\Support\Collection
     * */
    public function find($amoDomain)
    {
        return static::$collect->filter(
            $amoDomain instanceof Closure ? $amoDomain : function ($item) use ($amoDomain) {
                return $item['amocrm']->domain == $amoDomain;
            }
        );
    }

    /**
     * @param Closure|string $amoDomain
     * @return void
     * */
    public function remove($amoDomain): void
    {
        static::$collect = static::$collect->filter(
            $amoDomain instanceof Closure ? $amoDomain : function ($item) use ($amoDomain) {
                return $item['amocrm']->domain != $amoDomain;
            }
        );
    }

    /**
     * @return static
     * */
    public static function context()
    {
        return static::$context;
    }
}