<?php
/**
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 * */

namespace Lantana\Extensions\Anemone\Connectors;

use Anemone\Contracts\BeAuthConnector;
use Lantana\Extensions\Anemone\OAuth2Service;

class JStorageConnector implements BeAuthConnector
{
    /**
     * @var OAuth2Service $oauthService
     * */
    protected $oauthService;

    public function __construct()
    {
        $this->oauthService = OAuth2Service::context();
    }

    public function read(array $authData): array
    {
        $model = $this->oauthService->find($authData['domain'])->first();
        return isset($model) ? $model['oauth']->toArray() : [];
    }

    public function write(array $authData, array $refreshAuthData): bool
    {
        $model = $this->oauthService->find($authData['domain'])->first();
        if (isset($model)) {
            return $model['oauth']->mergeRefresh($refreshAuthData)->save();
        }

        return false;
    }
}