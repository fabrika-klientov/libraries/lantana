<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard;

use Lantana\Extensions\Anemone\OAuth2Service;
use Lantana\Extensions\Guard\Contracts\BeSharedAmocrm;
use Lantana\Extensions\Guard\Contracts\BeSharedApikeys;
use Lantana\Extensions\Guard\Contracts\BeSharedCustomerServices;
use Lantana\Extensions\Guard\Contracts\BeSharedPeriods;
use Lantana\Extensions\Guard\Exceptions\GuardingException;
use Illuminate\Support\Str;

/**
 * @method setSharedCustomerService(BeSharedCustomerServices $sharedCustomerServices)
 * @method setSharedAmocrm(BeSharedAmocrm $sharedAmocrm)
 * @method setSharedApikey(BeSharedApikeys $sharedApikeys)
 * @method setSharedPeriod(BeSharedPeriods $sharedPeriods)
 *
 * @property BeSharedApikeys $apikeys
 * @property BeSharedCustomerServices $customerServices
 * @property BeSharedAmocrm $amocrm
 * @property BeSharedPeriods $periods
 *
 * @property \Illuminate\Support\Collection $oauthCollect ['amocrm' => $sharedAmocrm, 'oauth' => $sharedOauth, 'integration' => $sharedIntegrations]
 * @property array|null $oauth ['amocrm' => $sharedAmocrm, 'oauth' => $sharedOauth, 'integration' => $sharedIntegrations]
 * @property array $forAnemoneClient
 * */
class GuardService
{
    /**
     * @var OAuth2Service $auth2Service
     * */
    private $auth2Service;
    /**
     * @var BeSharedCustomerServices|null $sharedCustomerServices
     * */
    private $sharedCustomerServices;
    /**
     * @var BeSharedAmocrm|null $sharedAmocrm
     * */
    private $sharedAmocrm;
    /**
     * @var BeSharedApikeys|null $sharedApikeys
     * */
    private $sharedApikeys;
    /**
     * @var BeSharedPeriods|null $sharedPeriods
     * */
    private $sharedPeriods;
    /**
     * @var string[] $contexts
     * */
    private $contexts = ['sharedCustomerServices', 'sharedAmocrm', 'sharedApikeys', 'sharedPeriods'];
    /**
     * @var self $context
     * */
    private static $context;

    /**
     * @param OAuth2Service $auth2Service
     * @param BeSharedCustomerServices $sharedCustomerServices is primary
     * @return void
     * */
    public function __construct(OAuth2Service $auth2Service, BeSharedCustomerServices $sharedCustomerServices = null)
    {
        $this->auth2Service = $auth2Service;
        $this->sharedCustomerServices = $sharedCustomerServices;
        static::$context = $this;
    }

    /**
     * @param string $token
     * @return $this
     * @throws GuardingException
     * */
    public function validApikey(string $token)
    {
        $apikey = $this->apikeys;

        if (empty($apikey)) {
            throw new GuardingException('Context type BeSharedApikey not defined');
        }

        if ($apikey->key != $token) {
            throw new GuardingException('Token Apikey does\'t valid');
        }

        if ($apikey->active != 1) {
            throw new GuardingException('Token Apikey is inactive');
        }

        return $this;
    }

    /**
     * @return $this
     * @throws GuardingException
     * */
    public function validPeriod()
    {
        $periods = $this->periods;

        if (empty($periods)) {
            throw new GuardingException('Context type BeSharedPeriod not defined');
        }

        if (!isset($periods->periods) || empty((array)$periods->periods)) {
            throw new GuardingException('Periods list is empty');
        }

        $currentTime = time();

        foreach ($periods->periods as $period) {
            if (isset($period->date_end)) {
                if ($currentTime < strtotime($period->date_end)) {
                    return $this;
                }
            }
        }

        throw new GuardingException('All periods is refused');
    }

    /**
     * @return $this
     * @throws GuardingException
     * *@deprecated
     */
    public function validAmocrm()
    {
        $amocrm = $this->amocrm;

        if (empty($amocrm)) {
            throw new GuardingException('Context type BeSharedAmocrm not defined');
        }

        if (isset($amocrm->domain, $amocrm->login, /*$amocrm->secret_key, $amocrm->account_id*//*, $amocrm->password*/)) {
            return $this;
        }

        throw new GuardingException('Data amocrm is null and does\'t valid');
    }

    /**
     * @return $this
     * @throws GuardingException
     * */
    public function validOauthAmocrm()
    {
        /**
         * @var array|null $oauth
         * */
        $oauth = $this->auth2Service->find($this->amocrm->{'domain'})->first();

        if (isset($oauth) && isset($oauth['oauth']->access_token)) {
            return $this;
        }

        throw new GuardingException('Model shared-oauth not exist or not provide [access_token]');
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|void
     * */
    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'setSharedCustomerService':
            case 'setSharedAmocrm':
            case 'setSharedApikey':
            case 'setSharedPeriod':
                [$shared] = $arguments;
                if ($shared instanceof BeSharedCustomerServices) {
                    $this->sharedCustomerServices = $shared;
                } elseif ($shared instanceof BeSharedAmocrm) {
                    $this->sharedAmocrm = $shared;
                } elseif ($shared instanceof BeSharedApikeys) {
                    $this->sharedApikeys = $shared;
                } elseif ($shared instanceof BeSharedPeriods) {
                    $this->sharedPeriods = $shared;
                }
        }
    }

    /**
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'apikeys':
            case 'customerServices':
            case 'amocrm':
            case 'periods':
                $property = Str::camel('shared-' . $name);
                if (isset($this->{$property})) {
                    return $this->{$property};
                }

                $getMethod = Str::camel('get-' . $property);
                foreach ($this->contexts as $context) {
                    if (isset($this->{$context}) && $property != $context) {
                        $this->{$property} = $this->{$context}->{$getMethod}();
                        if ($this->{$property}) {
                            return $this->{$property};
                        }
                    }
                }
                break;

            case 'oauthCollect':
                return $this->auth2Service->find($this->amocrm->{'domain'});

            case 'oauth':
                return $this->oauthCollect
                    ->reduce(
                        function ($result, $item) {
                            if (isset($result['integration']) && isset($item['integration'])) {
                                if (strtotime($item['integration']->created_at) > strtotime($result['integration']->created_at)) {
                                    return $item;
                                }

                                return $result;
                            }

                            return $result ?? $item;
                        },
                        null
                    );

            case 'forAnemoneClient':
                $model = $this->oauth;
                if (isset($model)) {
                    return [
                        "domain" => $model['amocrm']->domain,
                        "token_type" => $model['oauth']->token_type ?? 'Bearer',
                        "access_token" => $model['oauth']->access_token,
                        "refresh_token" => $model['oauth']->refresh_token,
                        "client_secret" => $model['integration']->secret,
                        "client_id" => $model['integration']->integration_uuid,
                        'redirect_uri' => $model['integration']->_links->redirect_uri->href ?? '',
                    ];
                }

                return $this->amocrm->getJSModel()->{'toArray'}(); // old version
        }

        return null;
    }

    /**
     * @return void
     * */
    public function clear()
    {
        $this->sharedCustomerServices = null;
        $this->sharedAmocrm = null;
        $this->sharedApikeys = null;
        $this->sharedPeriods = null;
    }

    /** get statical this context
     * @return self|null
     * */
    public static function context()
    {
        return static::$context;
    }
}
