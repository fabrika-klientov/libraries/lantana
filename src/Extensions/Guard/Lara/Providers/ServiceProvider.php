<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Lara\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Lantana\Extensions\Anemone\OAuth2Service;
use Lantana\Extensions\Guard\Exceptions\GuardingException;
use Lantana\Extensions\Guard\GuardService;

class ServiceProvider extends BaseServiceProvider
{
    protected static $prefix = 'api/v0';
    protected static $namespace = 'Lantana\Extensions\Guard\Lara\Controllers';

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRoutes();

        $this->app->singleton(
            OAuth2Service::class,
            function ($app) {
                return new OAuth2Service();
            }
        );

        $this->app->singleton(
            GuardService::class,
            function ($app) {
                return new GuardService($app->make(OAuth2Service::class));
            }
        );

        $distributionServiceClass = 'Anemone\Extensions\Distribution\DistributionService';
        if (class_exists($distributionServiceClass)) {
            $this->app->singleton(
                $distributionServiceClass,
                function ($app) use ($distributionServiceClass) {
                    /**
                     * @var GuardService $guardService
                     * */
                    $guardService = $app->make(GuardService::class);
                    $amoCrmContext = $guardService->amocrm;

                    if (is_null($amoCrmContext)) {
                        throw new GuardingException(
                            'Before using [' . $distributionServiceClass . '] init the [' . GuardService::class . ']'
                        );
                    }

                    $clientClass = 'Anemone\Client';

                    return new $distributionServiceClass(new $clientClass($amoCrmContext->getJSModel()->toArray()));
                }
            );
        }
    }

    /**
     * Register the Horizon routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group(
            [
                'prefix' => static::$prefix,
                'namespace' => static::$namespace,
            ],
            function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
            }
        );
    }
}
