<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.22
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Lara\Middlewares;

use Anemone\Extensions\Distribution\DistributionService;
use Anemone\Extensions\Distribution\Distribution as DistributionCore;
use Lantana\Extensions\Collection\Collection;
use Lantana\Extensions\Guard\GuardService;
use Closure;
use Lantana\Models\JSSharedDistributionConfigs;

class Distribution
{

    /**
     * @var GuardService $guardService
     * */
    protected $guardService;

    /**
     * @var DistributionService $distributionService
     * */
    protected $distributionService;

    /**
     * Create a new middleware instance.
     *
     * @param GuardService  $guardService
     * @return void
     */
    public function __construct(GuardService $guardService)
    {
        $this->guardService = $guardService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $this->distributionService = app()->make(DistributionService::class);

        if (is_null($this->guardService->customerServices->sharedDistributionConfigs)) {
            $collect = JSSharedDistributionConfigs::where('shared-customers-services_uuid', $this->guardService->customerServices->uuid)->get();
            $customerServices = $this->guardService->customerServices->getJSModel();
            if (!isset($customerServices->_embedded)) {
                $customerServices->_embedded = (object)[];
            }
            $customerServices->_embedded->{'shared-distribution-configs'} = $collect ?? null;
        }
        /**
         * @var Collection $sharedDistributionConfigs
         * */
        $sharedDistributionConfigs = $this->guardService->customerServices->sharedDistributionConfigs;

        if ($sharedDistributionConfigs && $sharedDistributionConfigs->isNotEmpty()) {
            $this->distributionService->init(new DistributionCore($sharedDistributionConfigs->first()->toArray()));
        }

        return $next($request);
    }
}
