<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Lara\Middlewares;

use Lantana\Extensions\Anemone\Connectors\JStorageConnector;
use Lantana\Extensions\Guard\Exceptions\GuardingException;
use Lantana\Extensions\Guard\Models\SharedCustomerServices;
use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedOauth;

class Authenticate extends BaseGuard
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     * */
    protected function isAuth($request): bool
    {
        $auth = explode(' ', $request->header('Authorization') ?? '');
        if (count($auth) < 2) {
            return false;
        }

        $token = array_pop($auth);
        try {
            $result = JSSharedApikeys::where('key', $token)->first();
            if (isset($result)) {
                $this->initGuardService($result);
                return true;
            }
        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }

        return false;
    }

    /** init service
     * @param JSSharedApikeys $apiKey
     * @throws \Exception
     * */
    protected function initGuardService($apiKey)
    {
        $shared = JSSharedCustomersServices::where('shared-apikeys_uuid', $apiKey->uuid)
            ->with('shared-apikeys')
            ->with('shared-amocrm')
            ->with('shared-periods');

        $this->loadOtherModels($shared);

        $model = $shared->first();

        if (isset($model)) {
            $oauthModels = $this->oauthLoadModels($model);
            $this->initOAuth($oauthModels, $model->sharedAmocrm);

            $this->guardService->setSharedCustomerService(new SharedCustomerServices($model));
            return;
        }

        throw new GuardingException('#AUTH:: JSSharedCustomersServices returned null');
    }

    /** helper lazy loading models
     * @param JSSharedCustomersServices $shared
     * */
    protected function loadOtherModels($shared)
    {
        if (!empty($this->appTables)) {
            foreach ($this->appTables as $table) {
                $shared->belongs($table);
            }
        }

        if (!empty($this->appModels)) {
            foreach ($this->appModels as $model) {
                $shared->belongsToModel($model);
            }
        }
    }

    /** helper load oauth models
     * @param JSSharedCustomersServices $customersServices
     * @return  \Illuminate\Support\Collection
     *
     * @throws \Lantana\Core\Exceptions\LantanaException
     */
    private function oauthLoadModels(JSSharedCustomersServices $customersServices)
    {
        $amocrm = $customersServices->sharedAmocrm;
        if (isset($amocrm) && getenv('A_AUTH_CONNECTOR') == JStorageConnector::class) {
            return JSSharedOauth::where('shared-amocrm_uuid', $amocrm->uuid)
                ->where('code', $customersServices->code)
                ->withSharedIntegrations()
                ->get();
        }

        return collect();
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param \Lantana\Models\JSSharedAmocrm $amocrm
     * */
    private function initOAuth($collection, $amocrm)
    {
        if ($collection->isNotEmpty()) {
            $collection->each(function (JSSharedOauth $sharedOauth) use ($amocrm) {
                $integration = $sharedOauth->sharedIntegrations();
                if (isset($integration)) {
                    $this->auth2Service->add($amocrm, $sharedOauth, $integration);
                }
            });
        }
    }
}
