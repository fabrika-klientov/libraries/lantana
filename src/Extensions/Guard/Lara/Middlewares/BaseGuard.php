<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */
namespace Lantana\Extensions\Guard\Lara\Middlewares;

use Lantana\Extensions\Anemone\OAuth2Service;
use Lantana\Extensions\Guard\GuardService;
use Closure;

abstract class BaseGuard
{
    /**
     * The authentication guard factory instance.
     *
     * @var GuardService
     */
    protected $guardService;
    /**
     * @var OAuth2Service $auth2Service
     * */
    protected $auth2Service;
    protected $appTables = [];
    protected $appModels = [];

    /**
     * Create a new middleware instance.
     *
     * @param  GuardService  $guardService
     * @param OAuth2Service $auth2Service
     * @return void
     */
    public function __construct(GuardService $guardService, OAuth2Service $auth2Service)
    {
        $this->guardService = $guardService;
        $this->auth2Service = $auth2Service;

        // init config app tables for eager loading
        $appTables = env('APP_CONFIG_TABLES', '');
        $appModels = env('APP_CONFIG_MODELS', '');
        $this->appTables = empty($appTables) ? [] : explode(',', $appTables);
        $this->appModels = empty($appModels) ? [] : explode(',', $appModels);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$this->isAuth($request)) {
            return response(['status' => false, 'message' => 'Unauthorized.'], 401);
        }

        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request  $request
     * @return bool
     * */
    abstract protected function isAuth($request): bool;
}
