<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 14.09.19
 * Time: 14:07
 *
 * Helper global functions
 *
 * Loaded from Composer autoload functions (files section)
 * !! First update in existing project when has this library then you should run >> composer dump-autoload
 *
 * Push new function should be unique in current app
 * recommending:
 * (l){FunctionName} -> (l)atoexst or (l)antana
 *
 * <3
 */


/** helper global function GuardService
 * @return \Lantana\Extensions\Guard\GuardService|null
 * */
function lGuard()
{
    return \Lantana\Extensions\Guard\GuardService::context();
}

/** helper global function get shared customers (Helper facade)
 * !! This function return NOT \Lantana\Models\JSSharedCustomersServices
 * (optional!! use getJSModel() interface Lantana\Extensions\Guard\Contracts\BeGuard for get)
 *
 * @return \Lantana\Extensions\Guard\Contracts\BeSharedCustomerServices|null
 * */
function lCustomer()
{
    $guardService = lGuard();

    if (isset($guardService)) {
        return $guardService->customerServices;
    }

    return null;
}
