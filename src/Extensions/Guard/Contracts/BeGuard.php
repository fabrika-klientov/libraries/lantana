<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Contracts;

interface BeGuard
{
    /**
     * @return BeMainGuardModel
     * */
    public function getJSModel();
}
