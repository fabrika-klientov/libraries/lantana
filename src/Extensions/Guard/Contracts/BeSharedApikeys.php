<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Contracts;

/**
 * @property string $key
 * @property int $active
 * */
interface BeSharedApikeys extends BeGuard
{
    /**
     * @return BeSharedCustomerServices
     * */
    public function getSharedCustomerServices();

    /**
     * @return BeSharedAmocrm
     * */
    public function getSharedAmocrm();

    /**
     * @return BeSharedPeriods
     * */
    public function getSharedPeriods();
}
