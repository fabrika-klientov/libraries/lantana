<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.9
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Exceptions;

class GuardingException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = 'LANTANA::GUARD: ' . $message;
    }
}
