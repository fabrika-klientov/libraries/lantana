<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Extensions\Guard\Models;

use Lantana\Extensions\Guard\Contracts\BeMainGuardModel;
use Lantana\Extensions\Guard\Contracts\BeSharedAmocrm;
use Lantana\Extensions\Guard\Contracts\BeSharedApikeys;
use Lantana\Extensions\Guard\Contracts\BeSharedCustomerServices;
use Lantana\Extensions\Guard\Contracts\BeSharedPeriods;

abstract class BaseModel
{
    /**
     * @var BeMainGuardModel $data
     * */
    protected $data;

    /**
     * @param BeMainGuardModel $mainGuardModel
     * @return void
     * */
    public function __construct(BeMainGuardModel $mainGuardModel)
    {
        $this->data = $mainGuardModel;
    }

    /**
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        if (isset($this->data->{$name})) {
            return $this->data->{$name};
        }

        return null;
    }

    /**
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data->{$name});
    }

    /**
     * @override
     * @return BeSharedAmocrm
     * */
    public function getSharedAmocrm()
    {
        return $this instanceof SharedAmocrm
            ? $this
            : $this->_getContext(SharedAmocrm::class, 'shared-amocrm');
    }

    /**
     * @override
     * @return BeSharedApikeys
     * */
    public function getSharedApikeys()
    {
        return $this instanceof SharedApikeys
            ? $this
            : $this->_getContext(SharedApikeys::class, 'shared-apikeys');
    }

    /**
     * @override
     * @return BeSharedPeriods
     * */
    public function getSharedPeriods()
    {
        return $this instanceof SharedPeriods
            ? $this
            : $this->_getContext(SharedPeriods::class, 'shared-periods');
    }

    /**
     * @override
     * @return BeSharedCustomerServices
     * */
    public function getSharedCustomerServices()
    {
        return $this instanceof SharedCustomerServices
            ? $this
            : $this->_getContext(SharedCustomerServices::class, 'shared-customers-services');
    }

    /** get init parent context model ex.: \Lantana\Models\JSSharedCustomersServices
     * @override
     * @return BeMainGuardModel|null
     * */
    public function getJSModel()
    {
        return $this->data;
    }

    /**
     * @param string $class
     * @param string $code
     * @return mixed
     * */
    private function _getContext($class, $code)
    {
        $model = $this->{$code};
        if (isset($model) && class_exists($class)) {
            return new $class($model);
        }

        return null;
    }
}
