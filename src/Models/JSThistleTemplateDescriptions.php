<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @category  JStorage
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.09.06
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $uid
 * */
class JSThistleTemplateDescriptions extends Model implements BeCollectionModel
{

}
