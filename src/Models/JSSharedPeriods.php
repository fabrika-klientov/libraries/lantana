<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Extensions\Guard\Contracts\BeMainGuardModel;
use Lantana\Model;

/**
 * Class JSSharedPeriods
 * @package Lantana\Models
 *
 * @property object|null $periods
 * @property-read object $active_period
 * @property-read array $allowed_periods
 */
class JSSharedPeriods extends Model implements BeMainGuardModel, BeCollectionModel
{
    /**
     * @var mixed $_active_period
     */
    protected $_active_period = null;

    /**
     * @var mixed $_allowed_periods
     */
    protected $_allowed_periods = null;

    public function __get($name)
    {
        $t = preg_split('/(?=[_])/', $name, -1, PREG_SPLIT_NO_EMPTY);
        $t = explode('_', $name);
        $t = 'get' . implode('', array_map(function ($v) {
            return ucfirst($v);
        }, $t));
        if (method_exists($this, $t)) {
            return $this->{$t}();
        }

        return parent::__get($name);
    }

    /**
     * @return mixed
     * */
    protected function getActivePeriod()
    {
        if ($this->_active_period) {
            return $this->_active_period;
        }
        if (is_null($this->periods)) {
            return null;
        }

        $tp = &$this->_active_period;
        $this->_allowed_periods = array_filter(
            (array)$this->periods,
            function ($p) use (&$tp) {
                $s = strtotime($p->date_start);
                $e = strtotime($p->date_end . " 23:59:59");
                if ($s <= time() && $e >= time()) {
                    !(!$tp || strtotime($tp->date_end) > $e) ?: ($tp = $p);
                    return true;
                }
                return false;
            }
        );
        if (empty($this->_allowed_periods)) {
            return null;
        }
        return $this->_active_period;
    }

    /**
     * @return mixed
     * */
    protected function getAllowedPeriods()
    {
        if (empty($this->_allowed_periods)) {
            if (!is_null($this->getActivePeriod())) {
                return $this->_allowed_periods;
            }
            return [];
        }

        return $this->_allowed_periods;
    }

    /**
     * lazy loading models
     * */

    /** get Collection<JSSharedCustomersServices>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedCustomersServices()
    {
        return $this->hasMany('Lantana\Models\JSSharedCustomersServices');
    }
}
