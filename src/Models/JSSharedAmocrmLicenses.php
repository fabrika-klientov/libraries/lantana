<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property bool $subscription_active
 * @property int $tariff_id
 * @property string $tariff_name
 * @property array|\stdClass $detail ex.:
 *   "idAccount": 16844260,
 *   "tariff": "Расширенный (Апрель 2018)",
 *   "period": "193 дней до 14.06.2020 21:00:00",
 *   "currency": "RUB",
 *   "users":{"bonusUsers": 0, "users": 9},
 *   "not_active_subscribe": false,
 *   "detail_tariff":{
 *     "id": 14911954,
 *     "name": "Расширенный (Апрель 2018)",
 *     "by_user": "Y",
 *     "lang": "RUB"
 *   }
 *
 * @property array|\stdClass $limits ex.:
 *   "current_leads": 8305,
 *   "current_contacts": 56590,
 *   "current_cf": 144,
 *   "max_leads": 9000,
 *   "max_contacts": 45000,
 *   "max_cf": 1800
 *
 * @property string $cost_uuid
 * ${shared-amocrm-licenses-cost_uuid}
 *
 */
class JSSharedAmocrmLicenses extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function belongsToAmocrm()
    {
        return $this->belongs("shared-amocrm");
    }

    /**
     * @return $this
     */
    public function withSharedAmocrmLicenses()
    {
        return $this->with("shared-amocrm-licenses-cost");
    }


    /**
     * lazy loading models
     * */

    /** get Collection<JSSharedAmocrm>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedAmocrm()
    {
        return $this->hasMany('Lantana\Models\JSSharedAmocrm');
    }

    /** get JSSharedAmocrmLicensesCost model
     * @return \Lantana\Models\JSSharedAmocrmLicensesCost
     * */
    public function sharedAmocrmLicensesCost()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrmLicensesCost');
    }
}
