<?php
/**
 * created by Jarvis
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $mode (disabled, logging, working, amocrm)
 * @property string $name
 * @property string $entry
 *
 * ${shared-customers-services_uuid}
 * ${shared-modules_uuid}
 * */
class JSBlackConfigs extends Model implements BeCollectionModel
{
    /**
     * @param array $data
     * @return JSBlackConfigs
     */
    public function merge(array $data)
    {
        $before = $this->toArray();
        unset($data['uuid']);
        $this->data = (object)array_replace_recursive($before, $data);

        return $this;
    }

    /**
     * @return $this
     */
    public function withSharedModules()
    {
        return $this->withModel(JSSharedModules::class);
    }

    /**
     * @return $this
     */
    public function withSharedCustomersServices()
    {
        return $this->withModel(JSSharedCustomersServices::class);
    }

    /**
     * @return $this
     */
    public function belongsToBlackRelationFields()
    {
        return $this->belongsToModel(JSBlackRelationFields::class);
    }

    // lazy

    /** get Collection<JSBlackRelationFields>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function blackRelationFields()
    {
        return $this->hasMany(JSBlackRelationFields::class);
    }
}
