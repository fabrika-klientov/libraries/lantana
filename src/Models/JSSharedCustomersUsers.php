<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

/**
 * Class JSSharedCustomersUsers
 * @package Lantana\Models
 *
 * @property string $updated_at
 * @property string $name
 * @property string $email
 * @property bool $is_admin
 * @property bool $active
 *
 */
class JSSharedCustomersUsers extends Model
{

    /**
     * lazy loading models
     * */

    /** get JSSharedAmocrm model
     * @return \Lantana\Models\JSSharedAmocrm
     * */
    public function sharedAmocrm()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrm');
    }
}
