<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   20.06.10
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

class JSSharedScripts extends Model
{
    /**
     * @return \Lantana\Models\JSSharedScripts
     */
    public function belongsToCustomerServices()
    {
        return $this
            ->belongs("shared-customers-services");
    }
}
