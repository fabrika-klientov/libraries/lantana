<?php

namespace Lantana\Models;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Core\Request\Request;
use Lantana\Model;

class Texts extends Model
{
    /**
     * @var string
     */
    protected $entity = 'texts';

    public function __construct(...$args)
    {
        parent::__construct($args);
        $this->data = (object)($args[0] ?? []);
        $this->data->{'locale'} ?? $this->setLocale('all');
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function asHtml($key)
    {
        $this->data->{$key} = 'html';
        return $this;
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function asPlain($key)
    {
        $this->data->{$key} = 'plain';
        return $this;
    }

    /**
     * @param string $locale

     * @return $this
     */
    public function setLocale($locale){
        $this->data->{'locale'} = $locale;
        return $this;
    }

    /**
     * @return array<mixed>|mixed
     * @throws LantanaException
     */
    final public function getTexts()
    {
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;
        $requestData->method('texts');
        $this->doRequest();
        //$this->data = $this->initDataCollection($this->data);
        //
        return $this->data;
    }


    /**
     * @throws LantanaException
     */
    private function doRequest():void 
    {
        /**
         * @var \Lantana\Core\Request\RequestParams $requestParams
         * */
        $requestParams = $this->request_params;
        /**
         * @var \Lantana\Core\Request\RequestData $requestData
         * */
        $requestData = $this->request_data;

        /**
         * @var string $uri
         * */
        $uri = $requestParams->uri();
        /**
         * @var string $path
         * */
        $path = $requestParams->path();

        $res = (new Request())
            ->url($uri . $path . $this->getEntity())
            ->data(array_merge(['data' => $this->data], $requestData->return()))
            ->post();
        if (isset($res->error) && $res->error) {
            $message = '';
            if (isset($res->message)) {
                $message = $res->message;
            }
            $lantanaException = new LantanaException('Response has error: ' . $message);
            $lantanaException->setData($res);
            throw $lantanaException;
        }
        foreach ($res as $k => &$v){
            if (is_array($v) || is_object($v)){
                foreach ((array)$v as $kk => &$vv){
                    ($vv = trim($vv)) ?: ($vv = "{{$k}}");
                }
            }
        }
        $this->data = $res;
    }
}
