<?php
/**
 * created by Jarvis
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property mixed $enterItem
 * @property mixed $intermediaryItem
 *
 * ${black-configs_uuid}
 * ${shared-customers-services_uuid}
 * */
class JSBlackRelationFields extends Model implements BeCollectionModel
{
    /**
     * @param array $data
     * @return $this
     */
    public function merge(array $data)
    {
        $before = $this->toArray();
        unset($data['uuid']);
        $this->data = (object)array_replace_recursive($before, $data);

        return $this;
    }

    /**
     * @return $this
     * */
    public function withBlackConfigs()
    {
        return $this->withModel(JSBlackConfigs::class);
    }

}
