<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.23
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * Move in Lantana
 * @property string $name
 * @property string $module
 * @property string $module_uuid
 * @property string $type
 * @property array|null $deep
 * ${shared-customers_uuid}
 * */
class JSLimeConvert extends Model implements BeCollectionModel
{

}
