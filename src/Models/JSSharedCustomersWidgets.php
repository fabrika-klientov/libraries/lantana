<?php

/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.3
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

/**
 * Class JSSharedCustomersWidgets
 * @package Lantana\Models
 *
 * @property string $updated_at
 * @property string $updated_at_amo
 * @property string $id
 * @property string $code
 * @property string $secret_key
 * @property string $version
 * @property int $installs
 * @property string $type
 * @property bool $active
 * @property string $name
 * @property string $description
 * @property string $categories
 * @property string $state
 * @property string $logo
 * @property array $settings
 *
 */

class JSSharedCustomersWidgets extends Model
{

    /**
     * lazy loading models
     * */

    /** get JSSharedAmocrm model
     * @return \Lantana\Models\JSSharedAmocrm
     * */
    public function sharedAmocrm()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrm');
    }
}
