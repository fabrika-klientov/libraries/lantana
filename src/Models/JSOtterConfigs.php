<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   JStorage
 * @category  Otter
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.07
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $type
 *
 * @property string $power
 * @property bool $list
 * @property string $for
 * @property array $rule
 *
 * @property string $shared-customers-services_uuid
 * */
class JSOtterConfigs extends Model implements BeCollectionModel
{

    /**
     * @param array $data
     * @return $this
     * */
    public function merge(array $data)
    {
        unset($data['uuid']);
        $this->data = (object)array_merge((array)$this->data, $data);

        return $this;
    }
}
