<?php
/**
 * created by Jarvis
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $title
 * @property string $code
 * @property bool $is_system
 * ${shared-customers-services_uuid}
 * */
class JSSharedIntermediaries extends Model implements BeCollectionModel
{

}
