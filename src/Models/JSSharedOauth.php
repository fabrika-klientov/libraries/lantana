<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * Class JSSharedOauth
 * @package Lantana\Models
 *
 * @property string $token_type
 * @property int $expires_in
 * @property string $access_token
 * @property string $refresh_token
 * @property int $time_at
 *
 * @property string $code (widget code) nullable
 *
 * @property string $shared\-amocrm_uuid
 * @property string $shared\-integrations_uuid
 *
 */
class JSSharedOauth extends Model implements BeCollectionModel
{

    /** merge data
     * @param array $data
     * @return $this
     * */
    public function mergeRefresh(array $data)
    {
        unset($data['uuid']);
        $this->data = (object)array_merge((array)$this->data, $data);

        return $this;
    }

    /**
     * @return $this
     * */
    public function withSharedAmocrm()
    {
        return $this->with('shared-amocrm');
    }

    /**
     * @return $this
     * */
    public function withSharedIntegrations()
    {
        return $this->with('shared-integrations');
    }


    /**
     * lazy loading models
     * */

    /** get JSSharedAmocrm model
     * @return \Lantana\Models\JSSharedAmocrm
     * */
    public function sharedAmocrm()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrm');
    }

    /** get JSSharedIntegrations model
     * @return \Lantana\Models\JSSharedIntegrations
     * */
    public function sharedIntegrations()
    {
        return $this->belongsTo('Lantana\Models\JSSharedIntegrations');
    }
}
