<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.22
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 *
 * */
class JSSharedDistributionConfigs extends Model implements BeCollectionModel
{

    /**
     * lazy loading models
     * */

    /** get JSSharedCustomersServices model
     * @return \Lantana\Models\JSSharedCustomersServices
     * */
    public function sharedCustomersServices()
    {
        return $this->belongsTo('Lantana\Models\JSSharedCustomersServices');
    }
}
