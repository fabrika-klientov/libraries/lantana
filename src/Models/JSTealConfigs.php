<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property object|null $configs
 * @property object|null $controls
 * */
class JSTealConfigs extends Model implements BeCollectionModel
{

    /**
     * lazy loading models
     * */

    /** get JSSharedCustomersServices model
     * @return \Lantana\Models\JSSharedCustomersServices|null
     * */
    public function sharedCustomersServices()
    {
        return $this->belongsTo('Lantana\Models\JSSharedCustomersServices');
    }
}
