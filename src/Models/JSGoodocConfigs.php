<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.5
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

/**
 * Class JSGoodocConfigs
 *
 * @package Lantana\Models
 *
 * @property string  $amo
 * @property string  $client_secret "JSON"
 * @property string  $credentials   "JSON"
 * @property object  $folders
 * @property object  $signatories
 * @property integer $responsibleSigner
 * @property integer $task_type
 */
class JSGoodocConfigs extends Model
{
    /**
     * @param array $data
     *
     * @return $this
     * @throws \Exception
     */
    public function findByAmoDomainAndUpdateOrCreateIfNeed($data)
    {
        $ret = $this->findByFieldValue('amo', $data['amo'])->first();
        if (empty($ret)) {
            $this->data = (object)$data;
            $this->save();
            return $this;
        }

        $reta = $ret->toArray();
        $merged = array_merge($reta, $data);
        if (strcmp((string)json_encode($reta), (string)json_encode($merged))!=0) {
            $this->data = (object)$merged;
            $this->save();
        }
        return $this;
    }
}
