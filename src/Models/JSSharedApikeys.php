<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Exception;
use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Extensions\Guard\Contracts\BeMainGuardModel;
use Lantana\Model;

/**
 * Class JSSharedApikeys
 *
 * @package Lantana\Models
 *
 * @property-read string                                                           $uuid
 * @property boolean                                                               $active
 * @property integer                                                               $created_at
 * @property integer                                                               $updated_at
 * @property-read \Lantana\Extensions\Collection\Collection<\Lantana\Models\JSSharedCustomersServices> $sharedCustomersServices
 */
class JSSharedApikeys extends Model implements BeMainGuardModel, BeCollectionModel
{
    /**
     *
     * @return \Lantana\Models\JSSharedApikeys
     */
    public function belongsToSharedCustomersServices()
    {
        return $this->belongs('shared-customers-services');
    }


    /**
     * lazy loading models
     * */

    /** get Collection<JSSharedCustomersServices>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedCustomersServices()
    {
        return $this->hasMany('Lantana\Models\JSSharedCustomersServices');
    }



    /**
     * ???
     * @return bool
     */
    public function isActive()
    {
        return (boolean)$this->active;
    }
}
