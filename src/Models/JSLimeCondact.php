<?php
/**
 * @package   Lime
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $name
 * @property string $type
 * @property string|null $extends
 * @property bool|null $extendsCondStatus
 * @property bool|null $condStatus
 * @property array $inner
 * ${lime-rules_uuid}
 * */
class JSLimeCondact extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function withLimeRules()
    {
        return $this->withModel(JSLimeRules::class);
    }
}
