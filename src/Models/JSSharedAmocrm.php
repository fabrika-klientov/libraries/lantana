<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.24
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Extensions\Guard\Contracts\BeMainGuardModel;
use Lantana\Model;

/**
 * Class JSSharedAmocrm
 *
 * @package Lantana\Models
 *
 * @property string  $domain
 * @property string  $login
 * @property string  $secret_key
 * @property string  $password
 * @property integer $account_id
 * @property bool    $confirmed
 * @property array   $licenses <DEPRECATED should use sharedAmocrmLicenses method for get>
 * @property string $licenses_uuid
 * ${shared-amocrm-licenses_uuid}
 * ${shared-modules_uuid}
 *
 * @method JSSharedAmocrm getByAccountID($field_value)
 */
class JSSharedAmocrm extends Model implements BeMainGuardModel, BeCollectionModel
{
    /**
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function belongsToCustomerServices()
    {
        return $this
            ->belongs("shared-customers-services");
    }

    /**
     * @deprecated
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function withSharedOauth()
    {
        return $this->with("shared-oauth");
    }

    /**
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function belongsToSharedCustomerWidgets()
    {
        return $this->belongs("shared-customers-widgets");
    }

    /**
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function belongsToSharedCustomerUsers()
    {
        return $this->belongs("shared-customers-users");
    }

    /**
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function withSharedAmocrmLicenses()
    {
        return $this->with("shared-amocrm-licenses");
    }

    /**
     * @return \Lantana\Models\JSSharedAmocrm
     */
    public function withSharedModules()
    {
        return $this->withModel(JSSharedModules::class);
    }

    /**
     * lazy loading models
     * */

    /** get Collection<JSSharedCustomersServices>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedCustomersServices()
    {
        return $this->hasMany('Lantana\Models\JSSharedCustomersServices');
    }

    /** get Collection<JSSharedCustomersWidgets>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedCustomerWidgets()
    {
        return $this->hasMany('Lantana\Models\JSSharedCustomersWidgets');
    }

    /** get Collection<JSSharedCustomersUsers>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedCustomerUsers()
    {
        return $this->hasMany('Lantana\Models\JSSharedCustomersUsers');
    }

    /** get model oauth
     * @return \Lantana\Models\JSSharedOauth
     * */
    public function sharedOauth()
    {
        return $this->belongsTo('Lantana\Models\JSSharedOauth');
    }

    /** get JSSharedAmocrmLicenses model
     * @return \Lantana\Models\JSSharedAmocrmLicenses
     * */
    public function sharedAmocrmLicenses()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrmLicenses');
    }

    // end lazy loading
}
