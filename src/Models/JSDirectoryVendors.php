<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   19.12.21
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

class JSDirectoryVendors extends Model
{
    /**
     * @return $this
     * */
    public function withSharedCustomersWidgets(): self
    {
        return $this->belongs('shared-customers-widgets');
    }

    /**
     * @return $this
     * */
    public function withAllRelations(): self
    {
        return $this
            ->withSharedCustomersWidgets();
    }
}
