<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.5
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string|null $stringField1
 * @property string|null $stringField2
 * @property array|null $arrayField1
 * @property array|null $arrayField2
 * @property bool|null $boolField1
 * @property bool|null $boolField2
 * @property int|null $intField1
 * @property int|null $intField2
 * @property object|null $objectField1
 * @property object|null $objectField2
 * */
class JSTest extends Model implements BeCollectionModel
{

}
