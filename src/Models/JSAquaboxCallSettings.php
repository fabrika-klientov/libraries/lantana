<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

/**
 * Class JSAquaboxCallSettings
 *
 * @package Lantana\Models
 *
 * @property string $call_system
 * @property int $company_id
 * @property \stdClass $option
 * @property mixed $default
 * @property string $call_settings_name_uuid
 * @property int $amo_id
 *
 */
class JSAquaboxCallSettings extends Model
{
}
