<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

/**
 * Class JSAquaboxScenario
 *
 * @package Lantana\Models
 *
 * @property string  $hash
 * @property object  $scenario
 * @property string  $name_id
 * @property integer $amo_id
 * @property string  $scenario_name_uuid
 */
class JSAquaboxScenario extends Model
{
    /**
     *
     * @return \Lantana\Models\JSAquaboxScenario
     */
    public function withAquaboxScenarioName()
    {
        return $this->with('aquabox-scenario-name', 'scenario_name_uuid');
    }
}
