<?php
/**
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property bool $power
 * @property string $customer_uuid
 * */
class JSSharedModules extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function belongsToAmocrm()
    {
        return $this->belongsToModel(JSSharedAmocrm::class);
    }

    /**
     * @return $this
     */
    public function belongsToBlackConfigs()
    {
        return $this->belongsToModel(JSBlackConfigs::class);
    }

    /**
     * @return $this
     */
    public function belongsToBisqueConfigs()
    {
        return $this->belongsToModel(JSBisqueConfigs::class);
    }

    /**
     * @return $this
     */
    public function belongsToAmberConfigs()
    {
        return $this->belongsToModel(JSAmberConfigs::class);
    }
}
