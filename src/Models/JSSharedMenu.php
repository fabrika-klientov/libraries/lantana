<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.11
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * Class JSSharedMenu
 *
 * @package Lantana\Models
 *
 * @property string|null $parent_uuid
 * @property string|null $icon
 * @property string $type [in enum 'element' | 'group']
 * @property string $title
 * @property string|null $link
 * @property bool $visible
 * @property bool $disabled
 * @property int $sort
 * @property array $roles
 */
class JSSharedMenu extends Model implements BeCollectionModel
{

}
