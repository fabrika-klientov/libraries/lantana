<?php
/**
 * @package   Lime
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.04.28
 * @link      https://fabrika-klientov.ua
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $name
 * @property bool $status
 * @property int $sort
 * @property string $event
 * @property array|mixed $eventData
 * @property bool $isInner
 *
 * ${shared-customers_uuid}
 * // ...
 * */
class JSLimeRules extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function belongsToLimeCondact()
    {
        return $this->belongsToModel(JSLimeCondact::class);
    }

    /**
     * lazy loading models
     * */

    /** get Collection<JSLimeCondact>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function limeCondact()
    {
        return $this->hasMany(JSLimeCondact::class);
    }
}
