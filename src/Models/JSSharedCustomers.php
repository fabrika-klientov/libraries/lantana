<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.23
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Illuminate\Support\Str;
use Lantana\Model;

/**
 * @property object|null $_embedded
 * */
class JSSharedCustomers extends Model
{
    /**
     * @return \Lantana\Models\JSSharedCustomers
     */
    public function belongsToAmocrm()
    {
        return $this
            ->belongs("shared-amocrm");
    }

    /**
     * @param array $relations
     * @return $this
     */
    public function getRelations(array $relations = ['shared-contacts'])
    {
        if (!$relations) {
            return $this;
        }
        $this->_embedded ?: $this->_embedded = new \stdClass();
//        is_array($relations) ?: $relations = [$relations]; // тут всегда будет массив иначе ошибка при вызове метода

        foreach ($relations as $v) {
            $class = '\App\JStorage\SPA\\JS' . Str::studly($v);
            $class = (new $class);
            if (!$this->{$v . '_uuids'} || !is_array($this->{$v . '_uuids'})) {
                $this->_embedded->{$v} = [];
                continue;
            }
            $this->_embedded->{$v} = $class->wherein('uuid', $this->{$v . '_uuids'})->get();
        }

        return $this;
    }
}
