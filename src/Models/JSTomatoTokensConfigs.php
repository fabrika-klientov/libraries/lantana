<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 21.11.19
 * Time: 15:06
 */

namespace Lantana\Models;


use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $shared-customers-services_uuid
 * @property string $name
 * @property bool $isDefault
 * @property string $token
 * @property bool $active
 * @property bool $status
 * @property int $number
 *
 * @property \Lantana\Extensions\Collection\Collection $customersConfigs
 * */
class JSTomatoTokensConfigs extends Model implements BeCollectionModel
{

    /**
     * lazy loading models
     * */

    /** get Collection<JSTomatoCustomersConfigs>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function customersConfigs()
    {
        return $this->hasMany('App\JSTomatoCustomersConfigs');
    }
}
