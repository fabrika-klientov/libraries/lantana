<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.5
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $tariff_name
 * @property array|\stdClass $tariff_cost ex.:
 *   "UAH": 599,
 *   "RUB": 999,
 *
 * @property array|\stdClass $limits ex.:
 *   "leads": 200,
 *   "contacts": 1000,
 *   "cf": 200
 * */
class JSSharedAmocrmLicensesCost extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function belongsToAmocrmLicenses()
    {
        return $this->belongs("shared-amocrm-licenses");
    }

    /**
     * lazy loading models
     * */

    /** get Collection<JSSharedAmocrmLicenses>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedAmocrmLicenses()
    {
        return $this->hasMany('Lantana\Models\JSSharedAmocrmLicenses');
    }
}
