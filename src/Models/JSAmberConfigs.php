<?php
/**
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.06.20
 * @link      https://fabrika-klientov.ua
 * */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $tracking_id
 * @property string $name
 * @property bool $status
 * @property bool $active
 *
 * @{shared-customers_uuid}
 * @{shared-modules_uuid}
 *
 * */
class JSAmberConfigs extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function withSharedModules()
    {
        return $this->withModel(JSSharedModules::class);
    }

    /**
     * @return $this
     */
    public function withSharedCustomers()
    {
        return $this->withModel(JSSharedCustomers::class);
    }
}
