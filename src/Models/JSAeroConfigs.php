<?php
/**
 *
 * @copyright   Fabrika-Klientov, 2018 {@link https://fabrika-klientov.ua}
 * @author      Vasiliy <php.iv@fabrika-klientov.com>
 * @author      Andrew <3oosor@gmail.com>
 * @package     Bisque
 * @version     2019.09.9
 *
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property mixed $data
 * @property array $rules
 * */
class JSAeroConfigs extends Model implements BeCollectionModel
{
}
