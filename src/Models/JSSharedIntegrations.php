<?php
/**
 *
 * @package   Lantana
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.03.14
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $integration_uuid extends of $uuid (for duplicates)
 * @property string $secret
 * @property string $name
 * @property string $description
 * @property int $state
 * @property string $updated_at
 * @property int $version_time
 * @property int $type
 * @property bool $is_editable
 * @property bool $has_widget
 * @property bool $is_enabled
 * @property bool $is_outdated
 * @property bool $has_logo
 * @property array $langs
 * @property array $_links
 * @property array $settings extends from _embedded (for conflict)
 * @property array $scopes extends from _embedded (for conflict)
 *
 * // widget data in integration (merge)
 * @property string $code
 * @property string $secret_key
 *
 * @property string $shared\-amocrm_uuid
 * @property string $shared\-widgets_uuid
 *
 * */
class JSSharedIntegrations extends Model implements BeCollectionModel
{
    /**
     * @return $this
     * */
    public function withSharedAmocrm()
    {
        return $this->with('shared-amocrm');
    }

    /**
     * @return $this
     * */
    public function withSharedWidgets()
    {
        return $this->with('shared-widgets');
    }

    /**
     * @return $this
     */
    public function belongsToOauth()
    {
        return $this->belongs("shared-oauth");
    }


    /**
     * lazy loading models
     * */

    /** get JSSharedAmocrm model
     * @return \Lantana\Models\JSSharedAmocrm
     * */
    public function sharedAmocrm()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrm');
    }

    /** get JSSharedWidgets model
     * @return \Lantana\Models\JSSharedWidgets
     * */
    public function sharedWidgets()
    {
        return $this->belongsTo('Lantana\Models\JSSharedWidgets');
    }

    /** get Collection<JSSharedOauth>
     * @return \Lantana\Extensions\Collection\Collection
     * */
    public function sharedOauth()
    {
        return $this->hasMany('Lantana\Models\JSSharedOauth');
    }
}