<?php
/**
 *
 * @copyright   Fabrika-Klientov, 2020 {@link https://fabrika-klientov.ua}
 * @author      Vasiliy <php.iv@fabrika-klientov.com>
 * @author      Andrew <3oosor@gmail.com>
 * @package     Bisque
 * @version     2020.05.12
 *
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $name
 * @property bool $status
 * @property bool $active
 * @property string $type
 * @property array|mixed $auth
 * @property string $responsible
 * @property string $last_fixed_date
 *
 * @{shared-customers-services_uuid}
 *
 * */
class JSBisqueConfigs extends Model implements BeCollectionModel
{

    /**
     * @return $this
     */
    public function withSharedModules()
    {
        return $this->withModel(JSSharedModules::class);
    }

    /**
     * @return $this
     */
    public function withSharedCustomersServices()
    {
        return $this->withModel(JSSharedCustomersServices::class);
    }
}
