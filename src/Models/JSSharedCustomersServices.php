<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.9.4
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Extensions\Guard\Contracts\BeMainGuardModel;
use Lantana\Model;

/**
 * Class JSSharedCustomersServices
 * @package Lantana\Models
 *
 * @property string $shared\-amocrm_uuid
 * @property string $shared\-apikeys_uuid
 * @property string $shared\-periods_uuid
 * @property string $code
 * @property \Lantana\Models\JSSharedAmocrm|null $sharedAmocrm
 * @property \Lantana\Models\JSSharedApikeys|null $sharedApikeys
 * @property \Lantana\Models\JSSharedPeriods|null $sharedPeriods
 * @property \Lantana\Models\JSSharedCustomers|null $sharedCustomers
 */
class JSSharedCustomersServices extends Model implements BeMainGuardModel, BeCollectionModel
{

    /**
     * @param string $code
     * @return $this
     * */
    public function findByCode($code)
    {
        return $this->findByFieldValue('code', $code);
    }

    /**
     * @return $this
     * */
    public function withSharedAmocrm()
    {
        return $this->with('shared-amocrm');
    }

    /**
     * @return $this
     * */
    public function withSharedApikeys()
    {
        return $this->with('shared-apikeys');
    }

    /**
     * @return $this
     * */
    public function withSharedPeriods()
    {
        return $this->with('shared-periods');
    }

    /**
     * @return $this
     * */
    public function withSharedCustomers()
    {
        return $this->with('shared-customers');
    }

    /**
     * @return $this
     * */
    public function withAllRelations()
    {
        return $this
            ->withSharedAmocrm()
            ->withSharedApikeys()
            ->withSharedPeriods()
            ->withSharedCustomers();
    }


    /**
     * lazy loading models
     * */

    /** get JSSharedApikeys model
     * @return \Lantana\Models\JSSharedApikeys
     * */
    public function sharedApikeys()
    {
        return $this->belongsTo('Lantana\Models\JSSharedApikeys');
    }

    /** get JSSharedAmocrm model
     * @return \Lantana\Models\JSSharedAmocrm
     * */
    public function sharedAmocrm()
    {
        return $this->belongsTo('Lantana\Models\JSSharedAmocrm');
    }

    /** get JSSharedPeriods model
     * @return \Lantana\Models\JSSharedPeriods
     * */
    public function sharedPeriods()
    {
        return $this->belongsTo('Lantana\Models\JSSharedPeriods');
    }

    /** get JSSharedPeriods model
     * @return \Lantana\Models\JSSharedCustomers
     * */
    public function sharedCustomers()
    {
        return $this->belongsTo('Lantana\Models\JSSharedCustomers');
    }
}
