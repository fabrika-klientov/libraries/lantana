<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   JStorage
 * @category  Jade
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.8
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Extensions\Collection\Contracts\BeCollectionModel;
use Lantana\Model;

/**
 * @property string $name
 * @property string $typeRule
 * @property bool $power
 * @property string $fromEntity
 * @property string $toEntity
 * @property bool $mainContact
 * @property object $filter
 * @property object $rules
 * @property string $shared-customers-services_uuid
 * */
class JSJadeConfigs extends Model implements BeCollectionModel
{

}
