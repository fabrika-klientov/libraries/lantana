<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   19.9.23
 * @link      https://fabrika-klientov.ua
 */

namespace Lantana\Models;

use Lantana\Model;

class JSSharedWidgets extends Model
{
    /**
     * @return \Lantana\Models\JSSharedWidgets
     */
    public function withAllRelations()
    {
        return $this
            ->withSharedWidgetsManifests()
            ->withSharedWidgetsSettings()
            ->withSharedWidgetsLocales();
    }

    /**
     * @return \Lantana\Models\JSSharedWidgets
     */
    public function withSharedWidgetsManifests()
    {
        return $this->with('shared-widgets-manifests');
    }

    /**
     * @return \Lantana\Models\JSSharedWidgets
     */
    public function withSharedWidgetsSettings()
    {
        return $this->with('shared-widgets-settings');
    }

    /**
     * @return \Lantana\Models\JSSharedWidgets
     */
    public function withSharedWidgetsLocales()
    {
        return $this->with('shared-widgets-locales');
    }

    /**
     * @return \Lantana\Models\JSSharedWidgets
     */
    public function belongsSharedIntegrations()
    {
        return $this->belongs('shared-integrations');
    }
}
