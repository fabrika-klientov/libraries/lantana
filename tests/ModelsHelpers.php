<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */
namespace Tests;


trait ModelsHelpers
{

    protected function getClassModel()
    {
        $class = last(explode('\\', get_class()));
        $class = str_replace('Test', '', $class);

        return class_exists('Lantana\\Models\\' . $class) ? 'Lantana\\Models\\' . $class : null;
    }

    /**
     * @override
     * */
    protected function setUp(): void
    {
        $class = $this->getClassModel();
        if (isset($class)) {
            $this->test = new $class($this->uri, $this->apikey);
        }
    }
}