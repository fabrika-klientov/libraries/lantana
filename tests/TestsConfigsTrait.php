<?php
/**
 *
 * @package   Lantana
 * @author    Vasiliy <php.iv@fabrika-klientov.com>
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.5
 * @link      https://fabrika-klientov.ua
 */

namespace Tests;

use Lantana\Models\JSTest;

trait TestsConfigsTrait
{
    /**
     * @var JSTest|\Lantana\Model|mixed $test
     * */
    protected $test;

    /**
     * @var string $uri
     * */
    protected $uri = 'https://jstorage.bpmcenter.pro';

    /**
     * @var string $apikey
     * */
    protected $apikey = 'b9b7089041493589bdaa81c079dba2f0';

    /**
     * @deprecated
     * */
    protected $class = [
        'JSNotExist' => 'class JSNotExist extends \Lantana\Base { }',
        'JSKeys' => 'class JSKeys extends \Lantana\Base { }',
    ];

    /**
     * @override
     * */
    protected function setUp(): void
    {
        $this->test = new JSTest($this->uri, $this->apikey);
    }

    /**
     * @override
     * */
    protected function tearDown(): void
    {
        $this->test = null;
    }

}
