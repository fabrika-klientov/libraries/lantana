<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Request;

use Lantana\Core\Request\RequestParams;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class RequestParamsTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new RequestParams();
        new RequestParams($this->uri);
        new RequestParams($this->uri, $this->apikey);

        $this->assertTrue(true);
    }

    public function test__call()
    {
        $this->assertEquals($this->uri, $this->test->uri());
        $this->assertInstanceOf(RequestParams::class, $this->test->uri('new_uri'));
    }

    public function testUri()
    {
        $this->assertEquals($this->uri, $this->test->uri());
        $this->assertInstanceOf(RequestParams::class, $this->test->uri('new_uri'));
    }

    public function testApikey()
    {
        $this->assertEquals($this->apikey, $this->test->apikey());
        $this->assertInstanceOf(RequestParams::class, $this->test->apikey('new_uri'));
        $this->assertEquals('new_uri', $this->test->apikey());
    }

    public function testPath()
    {
        $this->assertEquals('/api/v1/data/', $this->test->path());
        $this->assertInstanceOf(RequestParams::class, $this->test->path('new_path'));
        $this->assertEquals('new_path', $this->test->path());
    }

    protected function setUp(): void
    {
        $this->test = new RequestParams($this->uri, $this->apikey);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }

}
