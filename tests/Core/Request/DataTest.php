<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Request;

use Lantana\Core\Request\Data;
use PHPUnit\Framework\TestCase;

class DataTest extends TestCase
{
    protected $test;

    public function test__invoke()
    {
        $test = $this->test;
        $invoke = $test();

        $this->assertInstanceOf(Data::class, $invoke);
    }

    public function test__construct()
    {
        new Data();
        new Data(['try' => 'catch']);

        $this->assertTrue(true);
    }

    public function test__call()
    {
        $res = $this->test->modelData();

        $this->assertInstanceOf(Data::class, $res);
    }

    public function testReturn()
    {
        $ret = $this->test->return();

        $this->assertEmpty($ret);

        $this->test->where('try', 'catch');

        $this->assertNotEmpty($this->test->return());
    }

    public function testReset()
    {
        $this->assertEmpty($this->test->return());

        $this->test->where('try', 'catch');

        $this->assertNotEmpty($this->test->return());

        $this->test->reset();
        // не знаю
    }

    public function testModelData()
    {
        $this->assertInstanceOf(Data::class, $this->test->modelData());
    }

    public function testResponsewithmodel()
    {
        $this->test->responsewithmodel(true);

        $this->assertEquals(['responsewithmodel' => true], $this->test->return());

        $this->test->responsewithmodel(false);

        $this->assertEquals([], $this->test->return());
    }

    public function testWhere()
    {
        $this->test->where('try', 'catch');

        $this->assertEquals(['where' => [['try', 'catch']]], $this->test->return());

        $this->test->where('tryTwo', 'catchTwo')->where('other', 'value');

        $this->assertEquals(['where' => [
            ['try', 'catch'],
            ['tryTwo', 'catchTwo'],
            ['other', 'value'],
        ]], $this->test->return());
    }

    public function testWherein()
    {
        $this->test->wherein('try', ['catch', 'value']);

        $this->assertEquals(['wherein' => ['try', ['catch', 'value']]], $this->test->return());
    }

    public function testOnly()
    {
        $this->test = new Data(['with', 'only', 'belongs']);

        $this->test->only(['try']);

        $this->assertEquals(['only' => ['try']], $this->test->return());
    }

    public function testWith()
    {
        $this->test = new Data(['with', 'only', 'belongs']);

        $this->test->with('table', 'key')->with('table2', 'key2');

        $this->assertEquals(['with' => ['table' => 'key', 'table2' => 'key2']], $this->test->return());
    }

    public function testBelongs()
    {
        $this->test = new Data(['with', 'only', 'belongs']);

        $this->test->belongs('table', 'key')->belongs('table2', 'key2');

        $this->assertEquals(['belongs' => ['table' => 'key', 'table2' => 'key2']], $this->test->return());
    }

    public function testStrict()
    {
        $this->test->strict(true);

        $this->assertEquals(['strict' => true], $this->test->return());

        $this->test->strict(false);

        $this->assertEquals([], $this->test->return());
    }

    protected function setUp(): void
    {
        $this->test = new Data(['where', 'wherein', 'responsewithmodel', 'strict']);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
