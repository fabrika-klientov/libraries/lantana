<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Request;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Core\Request\Request;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class RequestTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new Request();
        new Request('link');
        new Request('link', []);

        $this->assertTrue(true);
    }

    public function test__call()
    {
        $this->assertEquals($this->uri, $this->test->url());

        $this->test->url('link');

        $this->assertEquals('link', $this->test->url());
    }

    public function testPost()
    {
        $this->expectException(LantanaException::class);

        $this->test->post();
    }

    public function testUrl()
    {
        $this->assertEquals($this->uri, $this->test->url());

        $this->test->url('link');

        $this->assertEquals('link', $this->test->url());
    }

    public function testData()
    {
        $this->assertIsArray($this->test->data());
        $this->assertEmpty($this->test->data());

        $this->test->data(['data' => 'value']);

        $this->assertEquals(['data' => 'value'], $this->test->data());
    }

    protected function setUp(): void
    {
        $this->test = new Request($this->uri, []);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
