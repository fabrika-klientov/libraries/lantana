<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Request;

use Lantana\Core\Request\Data;
use Lantana\Core\Request\RequestData;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class RequestDataTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new RequestData();
        new RequestData('store');
        new RequestData('store', $this->apikey);

        $this->assertTrue(true);
    }

    public function test__call()
    {
        $this->assertEquals($this->test->data(), $this->test->data('any'));
        $this->assertInstanceOf(Data::class, $this->test->data());
        $this->assertEquals($this->test->apply(), $this->test->apply('any'));
        $this->assertInstanceOf(Data::class, $this->test->apply());
        $this->assertNotEquals($this->test->method('get'), $this->test->method());
        $this->assertEquals('get', $this->test->method());
        $this->assertInstanceOf(RequestData::class, $this->test->method('test'));
    }

    public function testReturn()
    {
        $this->assertEquals([
            'method' => 'detail',
            'key' => $this->apikey,
        ], $this->test->return());
    }

    public function testMethod()
    {
        $this->assertEquals('detail', $this->test->method());
        $this->assertInstanceOf(RequestData::class, $this->test->method('test'));
        $this->assertEquals('test', $this->test->method());
    }

    public function testKey()
    {
        $this->assertEquals($this->apikey, $this->test->key());
        $this->assertInstanceOf(RequestData::class, $this->test->key('any_key'));
        $this->assertEquals('any_key', $this->test->key());
    }

    public function testUuid()
    {
        $this->assertEmpty($this->test->uuid());
        $this->assertInstanceOf(RequestData::class, $this->test->uuid('any_uuid'));
        $this->assertEquals('any_uuid', $this->test->uuid());
    }

    public function testData()
    {
        $this->assertEquals($this->test->data(), $this->test->data('any'));
        $this->assertInstanceOf(Data::class, $this->test->data());
    }

    public function testApply()
    {
        $this->assertEquals($this->test->apply(), $this->test->apply('any'));
        $this->assertInstanceOf(Data::class, $this->test->apply());
    }

    public function testResponsewithmodel()
    {
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $this->test = new RequestData('detail', $this->apikey);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
