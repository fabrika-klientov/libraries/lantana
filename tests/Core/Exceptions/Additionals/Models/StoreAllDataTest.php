<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Exceptions\Additionals\Models;

use Lantana\Core\Exceptions\Additionals\Models\StoreAllData;
use Lantana\Extensions\Collection\Collection;
use PHPUnit\Framework\TestCase;

class StoreAllDataTest extends TestCase
{

    public function test__construct()
    {
        new StoreAllData([]);
        $this->assertTrue(true);
    }

    public function testGetData()
    {
        $test = new StoreAllData(['data' => 'value']);

        $this->assertEquals(['data' => 'value'], $test->getData());
    }

    public function test__get()
    {
        $test = new StoreAllData((object)['data' => 'value']);

        $this->assertEmpty($test->skipped);
        $this->assertEmpty($test->stored);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->stored);

        $test = new StoreAllData((object)['skipped' => ['value'], 'stored' => ['value', 'other']]);

        $this->assertNotEmpty($test->skipped);
        $this->assertNotEmpty($test->stored);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->stored);
        $this->assertCount(1, $test->skipped);
        $this->assertCount(2, $test->stored);
    }
}
