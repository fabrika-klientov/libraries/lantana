<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Exceptions\Additionals\Models;

use Lantana\Core\Exceptions\Additionals\Models\UpdateAllData;
use Lantana\Extensions\Collection\Collection;
use PHPUnit\Framework\TestCase;

class UpdateAllDataTest extends TestCase
{

    public function test__construct()
    {
        new UpdateAllData([]);
        $this->assertTrue(true);
    }

    public function testGetData()
    {
        $test = new UpdateAllData(['data' => 'value']);

        $this->assertEquals(['data' => 'value'], $test->getData());
    }

    public function test__get()
    {
        $test = new UpdateAllData((object)['data' => 'value']);

        $this->assertEmpty($test->skipped);
        $this->assertEmpty($test->updated);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->updated);

        $test = new UpdateAllData((object)['skipped' => ['value'], 'updated' => ['value', 'other']]);

        $this->assertNotEmpty($test->skipped);
        $this->assertNotEmpty($test->updated);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->updated);
        $this->assertCount(1, $test->skipped);
        $this->assertCount(2, $test->updated);
    }
}
