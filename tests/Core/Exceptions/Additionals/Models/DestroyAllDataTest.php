<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Exceptions\Additionals\Models;

use Lantana\Core\Exceptions\Additionals\Models\DestroyAllData;
use Lantana\Extensions\Collection\Collection;
use PHPUnit\Framework\TestCase;

class DestroyAllDataTest extends TestCase
{
    public function test__construct()
    {
        new DestroyAllData([]);
        $this->assertTrue(true);
    }

    public function testGetData()
    {
        $test = new DestroyAllData(['data' => 'value']);

        $this->assertEquals(['data' => 'value'], $test->getData());
    }

    public function test__get()
    {
        $test = new DestroyAllData((object)['data' => 'value']);

        $this->assertEmpty($test->skipped);
        $this->assertEmpty($test->deleted);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->deleted);

        $test = new DestroyAllData((object)['skipped' => ['value'], 'deleted' => ['value', 'other']]);

        $this->assertNotEmpty($test->skipped);
        $this->assertNotEmpty($test->deleted);
        $this->assertInstanceOf(Collection::class, $test->skipped);
        $this->assertInstanceOf(Collection::class, $test->deleted);
        $this->assertCount(1, $test->skipped);
        $this->assertCount(2, $test->deleted);
    }
}
