<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Core\Exceptions\Additionals;

use Lantana\Core\Exceptions\Additionals\Additional;
use Lantana\Core\Exceptions\Additionals\Models\DestroyAllData;
use Lantana\Core\Exceptions\Additionals\Models\StoreAllData;
use Lantana\Core\Exceptions\Additionals\Models\UpdateAllData;
use PHPUnit\Framework\TestCase;

class AdditionalTest extends TestCase
{

    public function test__construct()
    {
        new Additional([]);
        $this->assertTrue(true);
    }

    public function testHandle()
    {
        $add = new Additional([]);

        $this->assertNull($add->handle());

        $add = new Additional(['stored' => ['data']]);

        $this->assertNull($add->handle());

        $add = new Additional((object)['stored' => ['data']]);

        $this->assertInstanceOf(StoreAllData::class, $add->handle());

        $add = new Additional((object)['updated' => ['data']]);

        $this->assertInstanceOf(UpdateAllData::class, $add->handle());

        $add = new Additional((object)['deleted' => ['data']]);

        $this->assertInstanceOf(DestroyAllData::class, $add->handle());
    }
}
