<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Extensions\Collection;

use Lantana\Extensions\Collection\Collection;
use Lantana\Models\JSTest;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class CollectionTest extends TestCase
{
    use TestsConfigsTrait;

    /**
     * @deprecated
     * */
    public function testInitialize()
    {
        $model = new JSTest($this->uri, $this->apikey, [['one'], ['two']]);
        $collection = Collection::initialize($model);

        $this->assertNotNull($collection);
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertCount(2, $collection);
    }

    public function testStore()
    {
        $test1 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test2 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        $this->test->store(); // need .env

        $test3 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test4 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        $this->assertGreaterThan($test1->count(), $test3->count());
//        $this->assertGreaterThan($test2->count(), $test4->count());
        $this->assertTrue(true);
    }

    public function testUpdate()
    {
        $test1 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test2 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        (new Collection([
//            $test1,
//            $test2,
//        ]))->update(); // need .env

        $test3 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test4 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        $this->assertGreaterThan($test1->count(), $test3->count());
//        $this->assertGreaterThan($test2->count(), $test4->count());
        $this->assertTrue(true);
    }

    public function testDestroy()
    {
        $test1 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test2 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        (new Collection([
//            $test1,
//            $test2,
//        ]))->destroy(); // need .env

        $test3 = (new JSTest($this->uri, $this->apikey))->where('stringField2', 'str 2')->get();
        $test4 = (new JSTest($this->uri, $this->apikey))->where('stringField1', 'str 1')->get();

//        $this->assertGreaterThan($test1->count(), $test3->count());
//        $this->assertGreaterThan($test2->count(), $test4->count());
        $this->assertTrue(true);
    }

    protected function setUp(): void
    {
        $this->test = new Collection([
            new JSTest($this->uri, $this->apikey, ['stringField2' => 'str 2']),
            new JSTest($this->uri, $this->apikey, ['stringField1' => 'str 1']),
        ]);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
