<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Extensions\Guard\Models;

use Lantana\Extensions\Guard\Models\SharedAmocrm;
use Lantana\Models\JSSharedAmocrm;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class SharedAmocrmTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new SharedAmocrm(new JSSharedAmocrm($this->uri, $this->apikey));

        $this->assertTrue(true);
    }
}
