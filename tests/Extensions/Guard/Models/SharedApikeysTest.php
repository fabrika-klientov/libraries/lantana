<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Extensions\Guard\Models;

use Lantana\Extensions\Guard\Models\SharedApikeys;
use Lantana\Models\JSSharedApikeys;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class SharedApikeysTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new SharedApikeys(new JSSharedApikeys($this->uri, $this->apikey));

        $this->assertTrue(true);
    }
}
