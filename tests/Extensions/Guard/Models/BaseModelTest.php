<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Extensions\Guard\Models;

use Lantana\Extensions\Guard\Models\BaseModel;
use Lantana\Extensions\Guard\Models\SharedAmocrm;
use Lantana\Extensions\Guard\Models\SharedApikeys;
use Lantana\Extensions\Guard\Models\SharedCustomerServices;
use Lantana\Extensions\Guard\Models\SharedPeriods;
use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedPeriods;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class BaseModelTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey));

        $this->assertTrue(true);
    }

    public function test__get()
    {
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->{'shared-amocrm'});
        $this->assertInstanceOf(JSSharedApikeys::class, $this->test->{'shared-apikeys'});
        $this->assertInstanceOf(JSSharedPeriods::class, $this->test->{'shared-periods'});
        $this->assertNull($this->test->{'shared-any'});
    }

    public function test__isset()
    {
        $this->assertTrue(isset($this->test->{'shared-amocrm'}));
        $this->assertTrue(isset($this->test->{'shared-apikeys'}));
        $this->assertTrue(isset($this->test->{'shared-periods'}));
        $this->assertFalse(isset($this->test->{'shared-any'}));
    }

    public function testGetSharedAmocrm()
    {
        $this->assertInstanceOf(SharedAmocrm::class, $this->test->getSharedAmocrm());
    }

    public function testGetSharedApikeys()
    {
        $this->assertInstanceOf(SharedApikeys::class, $this->test->getSharedApikeys());
    }

    public function testGetSharedPeriods()
    {
        $this->assertInstanceOf(SharedPeriods::class, $this->test->getSharedPeriods());
    }

    public function testGetSharedCustomerServices()
    {
        $this->assertInstanceOf(SharedCustomerServices::class, $this->test->getSharedCustomerServices());
    }

    public function testGetJSModel()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->getJSModel());
    }

    protected function setUp(): void
    {
        $this->test = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey, (object)[
            'shared-apikeys' => new JSSharedApikeys($this->uri, $this->apikey, (object)[
                'key' => 'valid',
                'active' => 1,
            ]),
            'shared-periods' => new JSSharedPeriods($this->uri, $this->apikey, (object)[
                'periods' => [(object)['date_end' => '2100-10-10']],
            ]),
            'shared-amocrm' => new JSSharedAmocrm($this->uri, $this->apikey, (object)[
                'domain' => 'jarvis.amocrm.com',
                'login' => 'jarvis',
                'secret_key' => 'jarvis_secret',
                'account_id' => 123456,
            ]),
        ]));
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
