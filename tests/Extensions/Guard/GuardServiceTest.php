<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Extensions\Guard;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Extensions\Anemone\OAuth2Service;
use Lantana\Extensions\Guard\Exceptions\GuardingException;
use Lantana\Extensions\Guard\GuardService;
use Lantana\Extensions\Guard\Models\SharedAmocrm;
use Lantana\Extensions\Guard\Models\SharedApikeys;
use Lantana\Extensions\Guard\Models\SharedCustomerServices;
use Lantana\Extensions\Guard\Models\SharedPeriods;
use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedPeriods;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class GuardServiceTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        $shared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey));
        new GuardService(new OAuth2Service(), $shared);
        $this->assertTrue(true);
    }

    public function testValidApikey()
    {
        $this->assertInstanceOf(GuardService::class, $this->test->validApikey('valid'));
    }

    public function testValidApikeyException()
    {
        $this->expectException(GuardingException::class);
        $this->assertInstanceOf(GuardService::class, $this->test->validApikey('no_valid'));
    }

    public function testValidPeriod()
    {
        $this->assertInstanceOf(GuardService::class, $this->test->validPeriod());
    }

    public function testValidPeriodException()
    {
        $shared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey, (object)[
            'shared-periods' => new JSSharedPeriods($this->uri, $this->apikey, (object)[
                'periods' => [(object)['date_end' => '2000-10-10']],
            ]),
        ]));
        $this->test = new GuardService(new OAuth2Service(), $shared);
        $this->expectException(GuardingException::class);
        $this->assertInstanceOf(GuardService::class, $this->test->validPeriod());
    }

    public function testValidAmocrm()
    {
        $this->assertInstanceOf(GuardService::class, $this->test->validAmocrm());
    }

    public function testValidAmocrmException()
    {
        $shared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey, (object)[
            'shared-amocrm' => new JSSharedAmocrm($this->uri, $this->apikey, (object)[
                'domain' => 'jarvis.amocrm.com',
//                'login' => 'jarvis',
                'secret_key' => 'jarvis_secret',
                'account_id' => 123456,
            ]),
        ]));
        $this->test = new GuardService(new OAuth2Service(), $shared);
        $this->expectException(GuardingException::class);
        $this->assertInstanceOf(GuardService::class, $this->test->validPeriod());
    }

    public function test__call()
    {
        $newShared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey));

        $this->assertNotEquals($newShared, $this->test->customerServices);

        $this->test->setSharedCustomerService($newShared);

        $this->assertEquals($newShared, $this->test->customerServices);
    }

    public function test__get()
    {
        $this->assertInstanceOf(SharedCustomerServices::class, $this->test->customerServices);
    }

    public function testContext()
    {
        $this->assertInstanceOf(GuardService::class, GuardService::context());
    }

    public function testSetSharedCustomerService()
    {
        $newShared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey));

        $this->assertNotEquals($newShared, $this->test->customerServices);

        $this->test->setSharedCustomerService($newShared);

        $this->assertEquals($newShared, $this->test->customerServices);
    }

    public function testSetSharedAmocrm()
    {
        $newShared = new SharedAmocrm(new JSSharedAmocrm($this->uri, $this->apikey));

        $this->assertNotEquals($newShared, $this->test->amocrm);

        $this->test->setSharedAmocrm($newShared);

        $this->assertEquals($newShared, $this->test->amocrm);
    }

    public function testSetSharedApikey()
    {
        $newShared = new SharedApikeys(new JSSharedApikeys($this->uri, $this->apikey));

        $this->assertNotEquals($newShared, $this->test->apikeys);

        $this->test->setSharedApikey($newShared);

        $this->assertEquals($newShared, $this->test->apikeys);
    }

    public function testSetSharedPeriod()
    {
        $newShared = new SharedPeriods(new JSSharedPeriods($this->uri, $this->apikey));

        $this->assertNotEquals($newShared, $this->test->periods);

        $this->test->setSharedPeriod($newShared);

        $this->assertEquals($newShared, $this->test->periods);
    }

    protected function setUp(): void
    {
        $shared = new SharedCustomerServices(new JSSharedCustomersServices($this->uri, $this->apikey, (object)[
            'shared-apikeys' => new JSSharedApikeys($this->uri, $this->apikey, (object)[
                'key' => 'valid',
                'active' => 1,
            ]),
            'shared-periods' => new JSSharedPeriods($this->uri, $this->apikey, (object)[
                'periods' => [(object)['date_end' => '2100-10-10']],
            ]),
            'shared-amocrm' => new JSSharedAmocrm($this->uri, $this->apikey, (object)[
                'domain' => 'jarvis.amocrm.com',
                'login' => 'jarvis',
                'secret_key' => 'jarvis_secret',
                'account_id' => 123456,
            ]),
        ]));
        $this->test = new GuardService(new OAuth2Service(), $shared);
    }

    protected function tearDown(): void
    {
        $this->test = null;
    }
}
