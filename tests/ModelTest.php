<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests;

use Lantana\Core\Exceptions\LantanaException;
use Lantana\Model;
use Lantana\Models\JSTest;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        $test = new JSTest($this->uri, $this->apikey);

        $this->assertInstanceOf(Model::class, $test);
        $this->assertInstanceOf(JSTest::class, $test);

        $this->assertEquals('null', (string)$test);
    }

    public function test__constructWithExceptions()
    {
        $this->expectException(LantanaException::class);
        $test = new JSTest();
    }

    public function test__set()
    {
        $this->test->stringField1 = 'string 1';
        $this->test->stringField2 = 'string 2';
        $this->test->arrayField1 = [1, 2];
        $this->test->objectField2 = (object)['filed' => 'value'];
        $this->test->boolField1 = true;
        $this->test->intField1 = 123;

        $this->assertEquals(json_encode([
            'stringField1' => 'string 1',
            'stringField2' => 'string 2',
            'arrayField1' => [1, 2],
            'objectField2' => (object)['filed' => 'value'],
            'boolField1' => true,
            'intField1' => 123,
        ]), (string)$this->test);
    }

    public function test__get()
    {
        $this->test__set();

        $this->assertEquals('string 1', $this->test->stringField1);
        $this->assertIsArray($this->test->arrayField1);
        $this->assertIsObject($this->test->objectField2);
        $this->assertObjectHasAttribute('filed', $this->test->objectField2);
        $this->assertTrue($this->test->boolField1);
        $this->assertLessThanOrEqual(123, $this->test->intField1);
    }

    public function test__isset()
    {
        $this->test__set();

        $this->assertTrue(isset($this->test->boolField1));
        $this->assertTrue(isset($this->test->objectField2));
        $this->assertFalse(isset($this->test->objectField1));
        $this->assertFalse(isset($this->test->intField2));
    }

    public function test__clone()
    {
        $clone = clone $this->test;

        $this->assertInstanceOf(JSTest::class, $clone);
        $this->assertTrue($clone !== $this->test);

        $noClone = $this->test;

        $this->assertTrue($noClone === $this->test);
    }

    public function testSave()
    {
        $this->test__set();

        $status = $this->test->save(); // store

        $this->assertTrue($status);

        $this->test->intField2 = 321;
        $status = $this->test->save(); // update

        $this->assertTrue($status);
    }

    public function testGet()
    {
        $collection = $this->test->get();

        $this->assertNotEmpty($collection);
    }

    public function testFindByUUID()
    {
        $model = $this->test->where('intField2', 321)->first();

        $this->assertNotNull($model);

        $forUuidModel = $this->test->findByUUID($model->uuid);

        $this->assertNotNull($forUuidModel);
        $this->assertEquals('string 1', $forUuidModel->stringField1);
    }

    public function testFirst()
    {
        $model = $this->test->where('stringField2', 'string 2')->first();

        $this->assertNotNull($model);
        $this->assertEquals('string 2', $model->stringField2);
    }

    public function testLast()
    {
        $model = $this->test->where('stringField2', 'string 2')->last();

        $this->assertNotNull($model);
        $this->assertEquals('string 2', $model->stringField2);
    }

    public function testToArray()
    {
        $this->test__set();

        $this->assertEquals([
            'stringField1' => 'string 1',
            'stringField2' => 'string 2',
            'arrayField1' => [1, 2],
            'objectField2' => ['filed' => 'value'],
            'boolField1' => true,
            'intField1' => 123,
        ], $this->test->toArray());
    }

    public function testWhere()
    {
        $collection = $this->test
            ->where('stringField2', 'string 2')
            ->where('stringField1', 'string 1')
            ->get();

        $this->assertNotNull($collection);
        $this->assertNotEmpty($collection);
    }

    public function testWherein()
    {
        $collection = $this->test
            ->wherein('stringField2', ['string 2'])
            ->get();

        $this->assertNotNull($collection);
        $this->assertNotEmpty($collection);
    }

    public function testSelect()
    {
        $collection = $this->test
            ->select(['stringField1', 'objectField2'])
            ->get();

        $this->assertNotNull($collection);
        $this->assertNotEmpty($collection);
        $collection->each(function (JSTest $item) {
            $this->assertNull($item->stringField2);
            $this->assertNull($item->intField1);
        });
    }

    public function testFind()
    {
        $model = $this->test->where('intField2', 321)->first();

        $this->assertNotNull($model);

        $forUuidModel = $this->test->find($model->uuid);

        $this->assertNotNull($forUuidModel);
        $this->assertEquals('string 1', $forUuidModel->stringField1);
    }

    public function testFindByFieldValue()
    {
        $model = $this->test->findByFieldValue('intField2', 321)->first();

        $this->assertNotNull($model);
    }

    public function testAll()
    {
        $collection = $this->test->all();

        $this->assertNotEmpty($collection);
    }

    public function testDelete()
    {
        $model = $this->test->where('stringField2', 'string 2')->last();

        $this->assertNotNull($model);
        $this->assertTrue($model->delete());
    }

}
