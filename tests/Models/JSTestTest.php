<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSTest;
use PHPUnit\Framework\TestCase;
use Tests\TestsConfigsTrait;

class JSTestTest extends TestCase
{
    use TestsConfigsTrait;

    public function test__construct()
    {
        $test = new JSTest($this->uri, $this->apikey);

        $this->assertInstanceOf(JSTest::class, $test);
    }
}
