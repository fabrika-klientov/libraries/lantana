<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.3
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSAquaboxScenario;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSAquaboxScenarioTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testName()
    {
        $this->assertTrue(true);
    }

    public function testAmo_id()
    {
        $this->assertTrue(true);
    }
}
