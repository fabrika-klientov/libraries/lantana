<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedCustomersWidgets;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedCustomersWidgetsTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testSharedAmocrm()
    {
        $this->test->{'shared-amocrm'} = new JSSharedAmocrm($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedCustomersWidgets::class, $this->test);
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->sharedAmocrm());
    }
}
