<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedCustomersServices;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedApikeysTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testBelongsToSharedCustomersServices()
    {
        $this->assertInstanceOf(JSSharedApikeys::class, $this->test->belongsToSharedCustomersServices());
    }

    public function testSharedCustomersServices()
    {
        $this->test->{'shared-customers-services'} = new JSSharedCustomersServices($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedApikeys::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->sharedCustomersServices());
    }

    public function testIsActive()
    {
        $this->assertFalse($this->test->isActive());

        $this->test->active = true;

        $this->assertTrue($this->test->isActive());
    }
}
