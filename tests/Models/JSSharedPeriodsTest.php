<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedPeriods;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedPeriodsTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function test__get()
    {
        $this->assertTrue(true);
    }

    public function testSharedCustomersServices()
    {
        $this->test->{'shared-customers-services'} = new JSSharedCustomersServices($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedPeriods::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->sharedCustomersServices());
    }
}
