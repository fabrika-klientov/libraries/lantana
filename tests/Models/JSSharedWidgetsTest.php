<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedWidgets;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedWidgetsTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testWithAllRelations()
    {
        $this->assertInstanceOf(JSSharedWidgets::class, $this->test->withAllRelations());
    }

    public function testWithSharedWidgetsManifests()
    {
        $this->assertInstanceOf(JSSharedWidgets::class, $this->test->withSharedWidgetsManifests());
    }

    public function testWithSharedWidgetsSettings()
    {
        $this->assertInstanceOf(JSSharedWidgets::class, $this->test->withSharedWidgetsSettings());
    }

    public function testWithSharedWidgetsLocales()
    {
        $this->assertInstanceOf(JSSharedWidgets::class, $this->test->withSharedWidgetsLocales());
    }
}
