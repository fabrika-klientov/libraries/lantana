<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedCustomersUsers;
use Lantana\Models\JSSharedCustomersWidgets;
use Lantana\Models\JSSharedOauth;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedAmocrmTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testBelongsToCustomerServices()
    {
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->belongsToCustomerServices());
    }

    public function testWithSharedOauth()
    {
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->withSharedOauth());
    }

    public function testBelongsToSharedCustomerWidgets()
    {
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->belongsToSharedCustomerWidgets());
    }

    public function testBelongsToSharedCustomerUsers()
    {
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->belongsToSharedCustomerUsers());
    }

    public function testSharedCustomersServices()
    {
        $this->test->{'shared-customers-services'} = new JSSharedCustomersServices($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->sharedCustomersServices());
    }

    public function testSharedCustomerWidgets()
    {
        $this->test->{'shared-customers-widgets'} = new JSSharedCustomersWidgets($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomersWidgets::class, $this->test->sharedCustomerWidgets());
    }

    public function testSharedCustomerUsers()
    {
        $this->test->{'shared-customers-users'} = new JSSharedCustomersUsers($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomersUsers::class, $this->test->sharedCustomerUsers());
    }

    public function testSharedOauth()
    {
        $this->test->{'shared-oauth'} = new JSSharedOauth($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test);
        $this->assertInstanceOf(JSSharedOauth::class, $this->test->sharedOauth());
    }

    public function testGetByAccountID()
    {
        $this->assertTrue(true);
    }
}
