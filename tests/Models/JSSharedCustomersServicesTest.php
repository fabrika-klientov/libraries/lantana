<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lantana
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.11.6
 * @link      https://fabrika-klientov.ua
 */

namespace Tests\Models;

use Lantana\Models\JSSharedAmocrm;
use Lantana\Models\JSSharedApikeys;
use Lantana\Models\JSSharedCustomers;
use Lantana\Models\JSSharedCustomersServices;
use Lantana\Models\JSSharedPeriods;
use PHPUnit\Framework\TestCase;
use Tests\ModelsHelpers;
use Tests\TestsConfigsTrait;

class JSSharedCustomersServicesTest extends TestCase
{
    use TestsConfigsTrait, ModelsHelpers {
        ModelsHelpers::setUp insteadof TestsConfigsTrait;
        TestsConfigsTrait::setUp as protected overSetUp;
    }

    public function testFindByCode()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->findByCode('any'));
    }

    public function testWithSharedAmocrm()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->withSharedAmocrm());
    }

    public function testWithSharedApikeys()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->withSharedApikeys());
    }

    public function testWithSharedPeriods()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->withSharedPeriods());
    }

    public function testWithSharedCustomers()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->withSharedCustomers());
    }

    public function testWithAllRelations()
    {
        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test->withAllRelations());
    }

    public function testSharedApikeys()
    {
        $this->test->{'shared-apikeys'} = new JSSharedApikeys($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test);
        $this->assertInstanceOf(JSSharedApikeys::class, $this->test->sharedApikeys());
    }

    public function testSharedAmocrm()
    {
        $this->test->{'shared-amocrm'} = new JSSharedAmocrm($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test);
        $this->assertInstanceOf(JSSharedAmocrm::class, $this->test->sharedAmocrm());
    }

    public function testSharedPeriods()
    {
        $this->test->{'shared-periods'} = new JSSharedPeriods($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test);
        $this->assertInstanceOf(JSSharedPeriods::class, $this->test->sharedPeriods());
    }

    public function testSharedCustomers()
    {
        $this->test->{'shared-customers'} = new JSSharedCustomers($this->uri, $this->apikey);

        $this->assertInstanceOf(JSSharedCustomersServices::class, $this->test);
        $this->assertInstanceOf(JSSharedCustomers::class, $this->test->sharedCustomers());
    }
}
